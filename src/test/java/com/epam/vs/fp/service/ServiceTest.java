package com.epam.vs.fp.service;

import com.epam.vs.fp.beans.*;
import com.epam.vs.fp.utility.comparator.ComparatorFactory;
import com.epam.vs.fp.utility.criteria.CommonSearchCriteria;
import com.epam.vs.fp.utility.criteria.SearchCriteria;
import com.epam.vs.fp.utility.parameter.GenreParameter;
import com.epam.vs.fp.utility.parameter.IdRangeParameter;
import com.epam.vs.fp.utility.parameter.NameParameter;
import com.epam.vs.fp.utility.parameter.Range;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.List;



public class ServiceTest {
    private static final ProductService service = ServiceFactory.INSTANCE.getProductService();

    @Test
    public void reverseTest() {
        List<Integer> list = List.of(1, 2, 3, 4, 5);
        Assertions.assertEquals(service.reverse(list), List.of(5, 4, 3, 2, 1));
    }

//    private <T extends PrintedProduct> void smallSortTest(List<T> elements, ComparatorFactory comFactory) {
//        List<T> sortedElements = service.sort(elements, comFactory);
//        T previous = sortedElements.get(0);
//        for (T product: sortedElements) {
//            Assertions.assertTrue(comFactory.getComparator().compare(previous, product) <= 0);
//            previous = product;
//        }
//    }

    @Test
    public void sortTest() {
        List<PrintedProduct> initialList = new LinkedList<>();
        initialList.add(new Book.Builder().id(1).genre("z").name("m").price(5).author("Yola").wordNumber(20).build());
        initialList.add(new Book.Builder().id(40).genre("a").name("w").price(10).author("Ala").wordNumber(5000).build());
        initialList.add(new Book.Builder().id(1).genre("z").name("m").price(5).author("Yola").wordNumber(20).build());
        initialList.add(new Picture.Builder().id(30).genre("s").name("a").price(0).author("Rio").colorState(Colorful.Color.BNW).build());
        initialList.add(new Document.Builder().id(50).name("b").price(10).wordNumber(100).build());

        List<PrintedProduct> sortedId = new LinkedList<>();
        sortedId.add(new Book.Builder().id(1).genre("z").name("m").price(5).author("Yola").wordNumber(20).build());
        sortedId.add(new Book.Builder().id(1).genre("z").name("m").price(5).author("Yola").wordNumber(20).build());
        sortedId.add(new Picture.Builder().id(30).genre("s").name("a").price(0).author("Rio").colorState(Colorful.Color.BNW).build());
        sortedId.add(new Book.Builder().id(40).genre("a").name("w").price(10).author("Ala").wordNumber(5000).build());
        sortedId.add(new Document.Builder().id(50).name("b").price(10).wordNumber(100).build());

        List<PrintedProduct> sortedName = new LinkedList<>();
        sortedName.add(new Picture.Builder().id(30).genre("s").name("a").price(0).author("Rio").colorState(Colorful.Color.BNW).build());
        sortedName.add(new Document.Builder().id(50).name("b").price(10).wordNumber(100).build());
        sortedName.add(new Book.Builder().id(1).genre("z").name("m").price(5).author("Yola").wordNumber(20).build());
        sortedName.add(new Book.Builder().id(1).genre("z").name("m").price(5).author("Yola").wordNumber(20).build());
        sortedName.add(new Book.Builder().id(40).genre("a").name("w").price(10).author("Ala").wordNumber(5000).build());


        List<PrintedProduct> sortedGenre = new LinkedList<>();
        sortedGenre.add(new Book.Builder().id(40).genre("a").name("w").price(10).author("Ala").wordNumber(5000).build());
        sortedGenre.add(new Picture.Builder().id(30).genre("s").name("a").price(0).author("Rio").colorState(Colorful.Color.BNW).build());
        sortedGenre.add(new Book.Builder().id(1).genre("z").name("m").price(5).author("Yola").wordNumber(20).build());
        sortedGenre.add(new Book.Builder().id(1).genre("z").name("m").price(5).author("Yola").wordNumber(20).build());
        sortedGenre.add(new Document.Builder().id(50).name("b").price(10).wordNumber(100).build());


        Assertions.assertEquals(sortedId, service.sort(initialList, ComparatorFactory.ID));
        Assertions.assertEquals(sortedName, service.sort(initialList, ComparatorFactory.NAME));
        Assertions.assertEquals(sortedGenre, service.sort(initialList, ComparatorFactory.GENRE));
    }

    @Test
    public void filterTest() {
        List<PrintedProduct> initialList = new LinkedList<>();
        initialList.add(new Book.Builder().id(1).genre("z").name("m").price(5).author("Yola").wordNumber(20).build());
        initialList.add(new Book.Builder().id(40).genre("s").name("w").price(10).author("Ala").wordNumber(5000).build());
        initialList.add(new Picture.Builder().id(30).genre("s").name("a").price(0).author("Rio").colorState(Colorful.Color.BNW).build());
        initialList.add(new Document.Builder().id(50).name("b").price(10).wordNumber(100).build());

        {
            List<PrintedProduct> genreFiltered = new LinkedList<>();
            genreFiltered.add(new Book.Builder().id(40).genre("s").name("w").price(10).author("Ala").wordNumber(5000).build());
            genreFiltered.add(new Picture.Builder().id(30).genre("s").name("a").price(0).author("Rio").colorState(Colorful.Color.BNW).build());
            SearchCriteria<PrintedProduct> genreCriteria = new CommonSearchCriteria();
            genreCriteria.add(new GenreParameter("s"));
            Assertions.assertEquals(genreFiltered, service.filter(initialList, genreCriteria));
        }
        {
            List<PrintedProduct> genreNameFiltered = new LinkedList<>();
            genreNameFiltered.add(new Book.Builder().id(40).genre("s").name("w").price(10).author("Ala").wordNumber(5000).build());

            SearchCriteria<PrintedProduct> genreNameCriteria = new CommonSearchCriteria();
            genreNameCriteria.add(new GenreParameter("s"));
            genreNameCriteria.add(new NameParameter("w"));
            Assertions.assertEquals(genreNameFiltered, service.filter(initialList, genreNameCriteria));
        }
        {
            List<PrintedProduct> idRangeFiltered = new LinkedList<>();
            idRangeFiltered.add(new Book.Builder().id(40).genre("s").name("w").price(10).author("Ala").wordNumber(5000).build());
            idRangeFiltered.add(new Picture.Builder().id(30).genre("s").name("a").price(0).author("Rio").colorState(Colorful.Color.BNW).build());

            SearchCriteria<PrintedProduct> idRangeCriteria = new CommonSearchCriteria();
            idRangeCriteria.add(new IdRangeParameter(new Range<>(20, 45)));
            Assertions.assertEquals(idRangeFiltered, service.filter(initialList, idRangeCriteria));
        }
    }
}
