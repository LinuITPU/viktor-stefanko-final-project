package com.epam.vs.fp.beans;

import org.junit.Test;


import static org.junit.Assert.assertThrows;

public class BeansTest {
    @Test
    public void bookThrowsWhenIgnoredNullableProperties() {
        for (int n = 0; n < 3; n++) {
            Book.Builder builder = new Book.Builder();
            builder.id(1);
            builder.price(18.5);
            builder.wordNumber(5000);
            if (n != 0) {builder.genre("fiction");}
            if (n != 1) {builder.author("Me");}
            if (n != 2) {builder.name("Any");}
            assertThrows(NullPointerException.class, builder::build);
        }
    }
    @Test
    public void documentThrowsWhenIgnoredNullableProperties() {
        Document.Builder builder = new Document.Builder();
        builder.id(1);
        builder.price(18.5);
        builder.wordNumber(5000);
        assertThrows(NullPointerException.class, builder::build);
    }
    @Test
    public void pictureThrowsWhenIgnoredNullableProperties() {
        for (int n = 0; n < 4; n++) {
            Picture.Builder builder = new Picture.Builder();
            builder.id(1);
            builder.price(18.5);
            if (n != 0) {builder.genre("fiction");}
            if (n != 1) {builder.colorState(Colorful.Color.COLORFUL);}
            if (n != 2) {builder.author("Me");}
            if (n != 3) {builder.name("Any");}
            assertThrows(NullPointerException.class, builder::build);
        }
    }
    @Test
    public void bookBuilds() {
        Book.Builder builder = new Book.Builder();
        builder.id(1);
        builder.price(18.5);
        builder.wordNumber(5000);
        builder.genre("fiction");
        builder.author("Me");
        builder.name("Any");
        builder.build();
    }
    @Test
    public void documentBuilds() {
        Document.Builder builder =new Document.Builder();
        builder.id(1);
        builder.price(18.5);
        builder.wordNumber(5000);
        builder.name("Any");
        builder.build();
    }
    @Test
    public void pictureBuilds() {
        Picture.Builder builder = new Picture.Builder();
        builder.id(1);
        builder.price(18.5);
        builder.colorState(Colorful.Color.BNW);
        builder.name("Any");
        builder.genre("fiction");
        builder.author("Me");
        builder.build();
    }

    @Test
    public void bookThrowsOnIllegalArguments() {
        Book.Builder builder = new Book.Builder();
        assertThrows(IllegalArgumentException.class, () -> builder.id(-5));
        assertThrows(IllegalArgumentException.class, () -> builder.price(-5));
        assertThrows(IllegalArgumentException.class, () -> builder.wordNumber(-5));
        assertThrows(NullPointerException.class, () -> builder.name(null));
        assertThrows(NullPointerException.class, () -> builder.name(""));
        assertThrows(NullPointerException.class, () -> builder.name(" "));
        assertThrows(NullPointerException.class, () -> builder.author(null));
        assertThrows(NullPointerException.class, () -> builder.author(""));
        assertThrows(NullPointerException.class, () -> builder.author(" "));
        assertThrows(NullPointerException.class, () -> builder.genre(null));
        assertThrows(NullPointerException.class, () -> builder.genre(""));
        assertThrows(NullPointerException.class, () -> builder.genre(" "));
    }
    @Test
    public void documentThrowsOnIllegalArguments() {
        Document.Builder builder =new Document.Builder();
        assertThrows(IllegalArgumentException.class, () -> builder.id(-5));
        assertThrows(IllegalArgumentException.class, () -> builder.price(-5));
        assertThrows(IllegalArgumentException.class, () -> builder.wordNumber(-5));
        assertThrows(NullPointerException.class, () -> builder.name(null));
        assertThrows(NullPointerException.class, () -> builder.name(""));
        assertThrows(NullPointerException.class, () -> builder.name(" "));
    }
    @Test
    public void pictureThrowsOnIllegalArguments() {
        Picture.Builder builder = new Picture.Builder();
        assertThrows(IllegalArgumentException.class, () -> builder.id(-5));
        assertThrows(IllegalArgumentException.class, () -> builder.price(-5));
        assertThrows(NullPointerException.class, () -> builder.name(null));
        assertThrows(NullPointerException.class, () -> builder.name(""));
        assertThrows(NullPointerException.class, () -> builder.name(" "));
        assertThrows(NullPointerException.class, () -> builder.author(null));
        assertThrows(NullPointerException.class, () -> builder.author(""));
        assertThrows(NullPointerException.class, () -> builder.author(" "));
        assertThrows(NullPointerException.class, () -> builder.genre(null));
        assertThrows(NullPointerException.class, () -> builder.genre(""));
        assertThrows(NullPointerException.class, () -> builder.genre(" "));
        assertThrows(NullPointerException.class, () -> builder.colorState(null));
    }
}
