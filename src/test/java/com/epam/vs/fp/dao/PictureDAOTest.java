package com.epam.vs.fp.dao;

import com.epam.vs.fp.beans.Picture;
import com.epam.vs.fp.beans.Colorful;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class PictureDAOTest {
    @Test
    public void createsAnEntry() {
        String[] strProperties = new String[]{"3", "any", "1221.3", "Person", "colorful", "barokko"};
        Picture expected = new Picture.Builder().id(3).name("any").price(1221.3).author("Person").colorState(Colorful.Color.COLORFUL).genre("barokko").build();
        AbstractDAO<Picture> dao = (AbstractDAO<Picture>) PictureDAO.getInstance();
        assertEquals(dao.convertStrArrToEntity(strProperties), expected);
    }
    @Test
    public void throwsOnShortArrays() {
        String[] strProperties = new String[]{"3", "any", "1221.3", "Person", "colorful", "barokko"};
        AbstractDAO<Picture> dao = (AbstractDAO<Picture>) PictureDAO.getInstance();
        for (int i = 5; i >= 0; i--) {
            String[] subArray = Arrays.copyOfRange(strProperties,0, i);
            assertThrows(IndexOutOfBoundsException.class, () -> dao.convertStrArrToEntity(subArray));
        }
    }

    @Test
    public void throwsOnWrongDataTypes() {
        String[] strProperties1 = new String[]{"hi", "any", "1221.3", "Person", "colorful", "barokko"};
        String[] strProperties2 = new String[]{"3", "any", "sup", "Person", "colorful", "barokko"};
        String[] strProperties3 = new String[]{"3", "any", "1221.3", "Person", "idk", "barokko"};
        AbstractDAO<Picture> dao = (AbstractDAO<Picture>) PictureDAO.getInstance();
        assertThrows(NumberFormatException.class, () -> dao.convertStrArrToEntity(strProperties1));
        assertThrows(NumberFormatException.class, () -> dao.convertStrArrToEntity(strProperties2));
        assertThrows(IllegalArgumentException.class, () -> dao.convertStrArrToEntity(strProperties3));
    }

    @Test
    public void throwsOnEmptyData() {
        String[] strProperties = new String[]{"3", "any", "1221.3", "Person", "colorful", "barokko"};
        AbstractDAO<Picture> dao = (AbstractDAO<Picture>) PictureDAO.getInstance();
        for (int i = 0; i < strProperties.length; i++) {
            String[] currentBadArray = Arrays.copyOf(strProperties, strProperties.length);
            currentBadArray[i] = "";
            assertThrows(NullPointerException.class,() -> dao.convertStrArrToEntity(currentBadArray));
        }
    }
}
