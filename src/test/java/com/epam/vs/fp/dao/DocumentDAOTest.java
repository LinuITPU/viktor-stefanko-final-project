package com.epam.vs.fp.dao;

import com.epam.vs.fp.beans.Document;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class DocumentDAOTest {
    @Test
    public void createsAnEntry() {
        String[] strProperties = new String[]{"3", "any", "1221.3", "210"};
        Document expected = new Document.Builder().id(3).name("any").price(1221.3).wordNumber(210).build();
        AbstractDAO<Document> dao = (AbstractDAO<Document>) DocumentDAO.getInstance();
        assertEquals(dao.convertStrArrToEntity(strProperties), expected);
    }
    @Test
    public void throwsOnShortArrays() {
        String[] strProperties = new String[]{"3", "any", "1221.3", "210"};
        AbstractDAO<Document> dao = (AbstractDAO<Document>) DocumentDAO.getInstance();
        for (int i = 3; i >= 0; i--) {
            String[] subArray = Arrays.copyOfRange(strProperties,0, i);
            assertThrows(IndexOutOfBoundsException.class, () -> dao.convertStrArrToEntity(subArray));
        }
    }

    @Test
    public void throwsOnWrongDataTypes() {
        String[] strProperties1 = new String[]{"any", "any", "1221.3", "210"};
        String[] strProperties2 = new String[]{"3", "any", "any", "210"};
        String[] strProperties3 = new String[]{"any", "any", "1221.3", "any"};
        String[][] strArrays = new String[][]{strProperties1, strProperties2, strProperties3};
        AbstractDAO<Document> dao = (AbstractDAO<Document>) DocumentDAO.getInstance();
        for (String[] strArr: strArrays) {
            assertThrows(NumberFormatException.class, () -> dao.convertStrArrToEntity(strArr));
        }
    }

    @Test
    public void throwsOnEmptyData() {
        String[] strProperties = new String[]{"3", "any", "1221.3", "210"};
        AbstractDAO<Document> dao = (AbstractDAO<Document>) DocumentDAO.getInstance();
        for (int i = 0; i < strProperties.length; i++) {
            String[] currentBadArray = Arrays.copyOf(strProperties, strProperties.length);
            currentBadArray[i] = "";
            assertThrows(NullPointerException.class,() -> dao.convertStrArrToEntity(currentBadArray));
        }
    }
}
