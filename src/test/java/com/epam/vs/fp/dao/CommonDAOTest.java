package com.epam.vs.fp.dao;

import com.epam.vs.fp.beans.Book;
import com.epam.vs.fp.beans.Document;
import com.epam.vs.fp.beans.Picture;
import com.epam.vs.fp.beans.PrintedProduct;
import org.junit.Test;
import org.opentest4j.AssertionFailedError;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;


public class CommonDAOTest {
    @Test
    public void testLoad() {
        Collection<PrintedProduct> loadedCommon = CommonDAO.getInstance().load();
        Collection<Book> loadedBook = BookDAO.getInstance().load();
        Collection<Document> loadedDocument = DocumentDAO.getInstance().load();
        Collection<Picture> loadedPicture = PictureDAO.getInstance().load();
        assertEquals(loadedCommon.size(), loadedPicture.size() + loadedBook.size() + loadedDocument.size());

        Collection<PrintedProduct> loadedSum = new LinkedList<>();
        loadedSum.addAll(loadedBook);
        loadedSum.addAll(loadedDocument);
        loadedSum.addAll(loadedPicture);
        for (PrintedProduct p: loadedCommon) {
            if (! loadedSum.remove(p)) {
                throw new AssertionFailedError();
            }
        }
    }
}
