package com.epam.vs.fp.dao;

import org.junit.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class DAOFactoryTest {
    @Test
    public void testClasses () {
        assertEquals(DAOFactory.BOOK.getDao().getClass(), BookDAO.class);
        assertEquals(DAOFactory.DOCUMENT.getDao().getClass(), DocumentDAO.class);
        assertEquals(DAOFactory.PICTURE.getDao().getClass(), PictureDAO.class);
        assertEquals(DAOFactory.COMMON.getDao().getClass(), CommonDAO.class);
    }
}
