package com.epam.vs.fp.dao;

import org.junit.Test;

import com.epam.vs.fp.beans.*;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;


public class BookDAOTest {
    @Test
    public void createsAnEntry() {
        String[] strProperties = new String[]{"3", "any", "1221.3", "210", "Person", "Fiction"};
        Book expected = new Book.Builder().id(3).name("any").price(1221.3).wordNumber(210).author("Person").genre("Fiction").build();
        AbstractDAO<Book> dao = (AbstractDAO<Book>) BookDAO.getInstance();
        assertEquals(dao.convertStrArrToEntity(strProperties), expected);
    }
    @Test
    public void throwsOnShortArrays() {
        String[] strProperties = new String[]{"3", "any", "1221.3", "210", "Person", "Fiction"};
        AbstractDAO<Book> dao = (AbstractDAO<Book>) BookDAO.getInstance();
        for (int i = 5; i >= 0; i--) {
            String[] subArray = Arrays.copyOfRange(strProperties,0, i);
            assertThrows(IndexOutOfBoundsException.class, () -> dao.convertStrArrToEntity(subArray));
        }
    }

    @Test
    public void throwsOnWrongDataTypes() {
        String[] strProperties1 = new String[]{"any", "any", "1221.3", "210", "Person", "Fiction"};
        String[] strProperties2 = new String[]{"3", "any", "any", "210", "Person", "Fiction"};
        String[] strProperties3 = new String[]{"any", "any", "1221.3", "any", "Person", "Fiction"};
        String[][] strArrays = new String[][]{strProperties1, strProperties2, strProperties3};
        AbstractDAO<Book> dao = (AbstractDAO<Book>) BookDAO.getInstance();
        for (String[] strArr: strArrays) {
            assertThrows(NumberFormatException.class, () -> dao.convertStrArrToEntity(strArr));
        }
    }

    @Test
    public void throwsOnEmptyData() {
        String[] strProperties = new String[]{"3", "any", "1221.3", "210", "Person", "Fiction"};
        AbstractDAO<Book> dao = (AbstractDAO<Book>) BookDAO.getInstance();
        for (int i = 0; i < strProperties.length; i++) {
            String[] currentBadArray = Arrays.copyOf(strProperties, strProperties.length);
            currentBadArray[i] = "";
            assertThrows(NullPointerException.class,() -> dao.convertStrArrToEntity(currentBadArray));
        }
    }
}
