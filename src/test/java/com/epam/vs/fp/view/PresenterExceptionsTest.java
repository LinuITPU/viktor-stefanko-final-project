package com.epam.vs.fp.view;

import com.epam.vs.fp.beans.PrintedProduct;
import com.epam.vs.fp.view.column.PriceColumn;
import org.junit.Test;

import java.util.LinkedList;

import static org.junit.Assert.assertThrows;

public class PresenterExceptionsTest {
    private static final ViewPresenter<PrintedProduct> presenter = WarehousePresenter.getINSTANCE();

    @Test
    public void outputEntriesThrowsNPE() {
        assertThrows(NullPointerException.class, () -> presenter.outputEntries(null, new PriceColumn(), true));
        assertThrows(NullPointerException.class, () -> presenter.outputEntries(new LinkedList<>(), null, true));
    }

    @Test
    public void printHelpThrowsNPE() {
        assertThrows(NullPointerException.class, () -> presenter.printHelp(null));
        assertThrows(NullPointerException.class, () -> presenter.printHelp(""));
        assertThrows(NullPointerException.class, () -> presenter.printHelp(" "));
    }
}
