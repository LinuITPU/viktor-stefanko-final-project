package com.epam.vs.fp.controller;

import com.epam.vs.fp.beans.*;
import com.epam.vs.fp.service.ServiceFactory;
import com.epam.vs.fp.view.column.Column;
import com.epam.vs.fp.view.column.ColumnFactory;
import org.junit.Test;

import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import static org.junit.Assert.*;

public class BrowseControllerTest {
    BrowseController controller = BrowseController.getINSTANCE();

    @Test
    public void executeDoesNotTakeArguments() {
        Queue<String> command = new LinkedList<>();
        command.add("Anything");
        assertThrows(InputMismatchException.class, () -> controller.execute(command));
    }

    @Test
    public void sort() {
        List<PrintedProduct> initialList = new LinkedList<>();
        initialList.add(new Book.Builder().id(1).genre("z").name("m").price(5).author("Yola").wordNumber(20).build());
        initialList.add(new Book.Builder().id(40).genre("s").name("w").price(10).author("Ala").wordNumber(5000).build());
        initialList.add(new Picture.Builder().id(30).genre("s").name("a").price(0).author("Rio").colorState(Colorful.Color.BNW).build());
        initialList.add(new Document.Builder().id(50).name("b").price(10).wordNumber(100).build());

        List<PrintedProduct> expected;
        expected = ServiceFactory.INSTANCE.getProductService().sort(initialList, BrowseController.getSortingAlgorithm().getColumnComparatorFactory());
        if (! BrowseController.isAscending()) {
            expected = ServiceFactory.INSTANCE.getProductService().reverse(expected);
        }
        assertEquals(expected, controller.sort(initialList));
    }

    @Test
    public void setSortingAlgorithm() {
        ColumnFactory[] columns = ColumnFactory.values();
        for (int i = 1; i < columns.length; i++) {
            if (columns[i-1].equals(ColumnFactory.NUMBER)){continue;}
            Queue<String> command = new LinkedList<>();

            command.add(columns[i - 1].getColumn().getColumnName());
            BrowseController.setSortingAlgorithm(command);
            Column firstAlgorithm = BrowseController.getSortingAlgorithm();

            command.add(columns[i].getColumn().getColumnName());
            BrowseController.setSortingAlgorithm(command);

            Column secondAlgorithm = BrowseController.getSortingAlgorithm();
            assertNotEquals(firstAlgorithm, secondAlgorithm);
        }
        Queue<String> command = new LinkedList<>();
        command.add("price");
        command.add("desc");
        BrowseController.setSortingAlgorithm(command);
        boolean firstAscending = BrowseController.isAscending();
        command.add("genre");
        BrowseController.setSortingAlgorithm(command);
        boolean secondAscending = BrowseController.isAscending();
        assertNotEquals(firstAscending, secondAscending);
    }

    @Test
    public void setSortingAlgorithmThrows() {
        {
            Queue<String> badCommand = new LinkedList<>();
            assertThrows(InputMismatchException.class, () -> BrowseController.setSortingAlgorithm(badCommand));
        }
        {
            Queue<String> badCommand = new LinkedList<>();
            badCommand.add("acsdi");
            assertThrows(InputMismatchException.class, () -> BrowseController.setSortingAlgorithm(badCommand));
            assertThrows(InputMismatchException.class, () -> BrowseController.setSortingAlgorithm(badCommand));
        }
        {
            Queue<String> badCommand = new LinkedList<>();
            badCommand.add("acsdi");
            badCommand.add("acsdi");
            assertThrows(InputMismatchException.class, () -> BrowseController.setSortingAlgorithm(badCommand));
        }
        {
            Queue<String> badCommand = new LinkedList<>();
            badCommand.add("price");
            badCommand.add("asCJN");
            assertThrows(InputMismatchException.class, () -> BrowseController.setSortingAlgorithm(badCommand));
        }
        {
            Queue<String> badCommand = new LinkedList<>();
            badCommand.add("price");
            badCommand.add("desc");
            badCommand.add("ashj");
            assertThrows(InputMismatchException.class, () -> BrowseController.setSortingAlgorithm(badCommand));
        }
        {
            Queue<String> badCommand = new LinkedList<>();
            badCommand.add("");
            assertThrows(InputMismatchException.class, () -> BrowseController.setSortingAlgorithm(badCommand));
        }
        {
            Queue<String> badCommand = new LinkedList<>();
            badCommand.add(" ");
            assertThrows(InputMismatchException.class, () -> BrowseController.setSortingAlgorithm(badCommand));
        }
        {
            Queue<String> badCommand = new LinkedList<>();
            badCommand.add("");
            assertThrows(InputMismatchException.class, () -> BrowseController.setSortingAlgorithm(badCommand));
        }
    }
}
