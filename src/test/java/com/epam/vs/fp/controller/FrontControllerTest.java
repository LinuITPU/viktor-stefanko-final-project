package com.epam.vs.fp.controller;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class FrontControllerTest {
    private static final FrontController controller = FrontControllerImpl.getINSTANCE();

    @Test
    public void exitsOnCommand() {
        assertFalse(controller.listen("exit"));
    }

    @Test
    public void ignoresEmpty() {
        assertTrue(controller.listen(null));
        assertTrue(controller.listen(""));
        assertTrue(controller.listen(" "));
    }

    @Test
    public void continuesOnCommands() {
        assertTrue(controller.listen("browse"));
        assertTrue(controller.listen("sort price"));
        assertTrue(controller.listen("sort price desc"));
        assertTrue(controller.listen("help"));
        assertTrue(controller.listen("help search"));
        assertTrue(controller.listen("search id 5"));
        assertTrue(controller.listen("search id range 4 7"));
    }

    @Test
    public void recoversFromWrongInputs() {
        assertTrue(controller.listen("ytchkgnm"));
        assertTrue(controller.listen("browse hi"));
        assertTrue(controller.listen("sort anyvbehjsd"));
        assertTrue(controller.listen("sort price akjs"));
        assertTrue(controller.listen("sort price desc hh"));
        assertTrue(controller.listen("search"));
        assertTrue(controller.listen("search price"));
        assertTrue(controller.listen("search price 100 20"));
        assertTrue(controller.listen("search price range 100"));
        assertTrue(controller.listen("search price range 100 9 9"));
        assertTrue(controller.listen("search price range -5 9"));
        assertTrue(controller.listen("search price -10"));
        assertTrue(controller.listen("search word-number -10"));
        assertTrue(controller.listen("search id -10"));
        assertTrue(controller.listen("help sdvsd"));
        assertTrue(controller.listen("help sort kjsv"));
    }

}
