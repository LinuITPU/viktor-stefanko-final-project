package com.epam.vs.fp.controller;

import org.junit.Test;

import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.Queue;

import static org.junit.Assert.assertThrows;

public class FilterControllerTest {
    private final static SmallController controller = FilterController.getINSTANCE();

    @Test
    public void throwsOnInvalid() {
        {
            Queue<String> badCommand = new LinkedList<>();
            assertThrows(InputMismatchException.class, () -> controller.execute(badCommand));
        }
        {
            Queue<String> badCommand = new LinkedList<>();
            badCommand.add("");
            assertThrows(InputMismatchException.class, () -> controller.execute(badCommand));
        }
        {
            Queue<String> badCommand = new LinkedList<>();
            badCommand.add("     ");
            assertThrows(InputMismatchException.class, () -> controller.execute(badCommand));
        }
        {
            Queue<String> badCommand = new LinkedList<>();
            badCommand.add("oasjbscasdk");
            assertThrows(InputMismatchException.class, () -> controller.execute(badCommand));
        }
        {
            Queue<String> badCommand = new LinkedList<>();
            badCommand.add("price");
            assertThrows(InputMismatchException.class, () -> controller.execute(badCommand));
        }
        {
            Queue<String> badCommand = new LinkedList<>();
            badCommand.add("100");
            assertThrows(InputMismatchException.class, () -> controller.execute(badCommand));
        }
        {
            Queue<String> badCommand = new LinkedList<>();
            badCommand.add("price");
            badCommand.add("range");
            assertThrows(InputMismatchException.class, () -> controller.execute(badCommand));
        }
        {
            Queue<String> badCommand = new LinkedList<>();
            badCommand.add("price");
            badCommand.add("Hi");
            assertThrows(InputMismatchException.class, () -> controller.execute(badCommand));
        }
        {
            Queue<String> badCommand = new LinkedList<>();
            badCommand.add("price");
            badCommand.add("range");
            assertThrows(InputMismatchException.class, () -> controller.execute(badCommand));
        }
        {
            Queue<String> badCommand = new LinkedList<>();
            badCommand.add("price");
            badCommand.add("range");
            badCommand.add("-1");
            badCommand.add("1");
            assertThrows(InputMismatchException.class, () -> controller.execute(badCommand));
        }
        {
            Queue<String> badCommand = new LinkedList<>();
            badCommand.add("price");
            badCommand.add("range");
            badCommand.add("10");
            badCommand.add("5");
            assertThrows(InputMismatchException.class, () -> controller.execute(badCommand));
        }
        {
            Queue<String> badCommand = new LinkedList<>();
            badCommand.add("price");
            badCommand.add("range");
            badCommand.add("hi");
            badCommand.add("hey");
            assertThrows(InputMismatchException.class, () -> controller.execute(badCommand));
        }
        {
            Queue<String> badCommand = new LinkedList<>();
            badCommand.add("price");
            badCommand.add("range");
            badCommand.add("8");
            badCommand.add("hey");
            assertThrows(InputMismatchException.class, () -> controller.execute(badCommand));
        }
        {
            Queue<String> badCommand = new LinkedList<>();
            badCommand.add("price");
            badCommand.add("100");
            badCommand.add("8");
            assertThrows(InputMismatchException.class, () -> controller.execute(badCommand));
        }
        {
            Queue<String> badCommand = new LinkedList<>();
            badCommand.add("price");
            badCommand.add("range");
            badCommand.add("100");
            badCommand.add("8");
            badCommand.add("8");
            assertThrows(InputMismatchException.class, () -> controller.execute(badCommand));
        }
        {
            Queue<String> badCommand = new LinkedList<>();
            badCommand.add("price");
            badCommand.add("range");
            badCommand.add("-100");
            badCommand.add("8");
            assertThrows(InputMismatchException.class, () -> controller.execute(badCommand));
        }
        {
            Queue<String> badCommand = new LinkedList<>();
            badCommand.add("price");
            badCommand.add("-100");
            assertThrows(InputMismatchException.class, () -> controller.execute(badCommand));
        }
        {
            Queue<String> badCommand = new LinkedList<>();
            badCommand.add("word-number");
            badCommand.add("-100");
            assertThrows(InputMismatchException.class, () -> controller.execute(badCommand));
        }
        {
            Queue<String> badCommand = new LinkedList<>();
            badCommand.add("id");
            badCommand.add("-100");
            assertThrows(InputMismatchException.class, () -> controller.execute(badCommand));
        }
    }
}
