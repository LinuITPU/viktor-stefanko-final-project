package com.epam.vs.fp.utility.comparator;

import com.epam.vs.fp.beans.*;
import org.junit.Test;

import static org.junit.Assert.*;

public class GenreComparatorTest {
    private static final PrintedProduct suitableProduct1 = new Book.Builder().author("Abc").genre("AAA").id(2).wordNumber(300).price(400).name("any").build();
    private static final PrintedProduct suitableProduct2 = new Picture.Builder().author("Abc").genre("ZZZ").id(2).colorState(Colorful.Color.BNW).price(400).name("any").build();
    private static final PrintedProduct unSuitableProduct = new Document.Builder().id(2).wordNumber(300).price(400).wordNumber(400).name("any").build();

    @Test
    public void testIsSuitable() {
        assertTrue(new GenreComparator().isSuitable(suitableProduct1));
        assertTrue(new GenreComparator().isSuitable(suitableProduct2));
        assertFalse(new GenreComparator().isSuitable(unSuitableProduct));
    }

    @Test
    public void Compare() {
        hasGenre p1 = (hasGenre) suitableProduct1;
        hasGenre p2 = (hasGenre) suitableProduct2;
        assertEquals(new GenreComparator().compare(p1, p2), CharSequence.compare(p1.getGenre(), p2.getGenre()));
        assertEquals(new GenreComparator().compare(p2, p1), CharSequence.compare(p2.getGenre(), p1.getGenre()));
        assertEquals(new GenreComparator().compare(p1, p1), 0);
    }
}
