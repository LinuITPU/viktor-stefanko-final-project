package com.epam.vs.fp.utility.parameter;

import com.epam.vs.fp.beans.*;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ColorParameterTest {

    @Test
    public void throwsWhenNull() {
        assertThrows(NullPointerException.class, () -> new ColorParameter(null));
    }

    @Test
    public void throwsWhenWrongClassProvided() {
        PrintedProduct wrongClass = new Document.Builder().id(1).wordNumber(2).name("fii").price(60).build();
        assertThrows(ClassCastException.class, () -> new ColorParameter(Colorful.Color.BNW).test(wrongClass));
    }

    @Test
    public void testTest() {
        PrintedProduct picture = new Picture.Builder().id(1).colorState(Colorful.Color.BNW).name("fii").price(60).author("anyone").genre("fic").build();
        PrintedProduct picture_ = new Picture.Builder().id(1).colorState(Colorful.Color.COLORFUL).name("fii").price(60).author("anyone").genre("fic").build();
        Parameter parameter = new ColorParameter(Colorful.Color.BNW);
        Parameter parameter_ = new ColorParameter(Colorful.Color.COLORFUL);
        assertTrue(parameter.test(picture));
        assertFalse(parameter.test(picture_));
        assertFalse(parameter_.test(picture));
        assertTrue(parameter_.test(picture_));
    }
}
