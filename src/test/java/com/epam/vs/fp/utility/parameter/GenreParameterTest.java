package com.epam.vs.fp.utility.parameter;

import com.epam.vs.fp.beans.*;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class GenreParameterTest {

    @Test
    public void throwsWhenNullOrEmpty() {
        assertThrows(NullPointerException.class, () -> new GenreParameter(null));
        assertThrows(NullPointerException.class, () -> new GenreParameter(""));
        assertThrows(NullPointerException.class, () -> new GenreParameter("  "));
    }

    @Test
    public void throwsWhenWrongClassProvided() {
        PrintedProduct wrongClass = new Document.Builder().id(1).wordNumber(2).name("fii").price(60).build();
        assertThrows(ClassCastException.class, () -> new GenreParameter("any").test(wrongClass));
    }

    @Test
    public void testTest() {
        PrintedProduct book = new Book.Builder().id(1).wordNumber(2).name("fii").price(60).author("anyone").genre("fic").build();
        PrintedProduct picture = new Picture.Builder().id(1).colorState(Colorful.Color.BNW).name("fii").price(60).author("anyone").genre("fic").build();
        Parameter parameter = new GenreParameter("fic");
        Parameter parameter_ = new GenreParameter("foo");
        assertTrue(parameter.test(book));
        assertTrue(parameter.test(picture));
        assertFalse(parameter_.test(book));
        assertFalse(parameter_.test(picture));
    }
}
