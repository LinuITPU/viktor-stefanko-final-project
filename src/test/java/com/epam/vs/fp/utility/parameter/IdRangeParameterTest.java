package com.epam.vs.fp.utility.parameter;

import com.epam.vs.fp.beans.Book;
import com.epam.vs.fp.beans.PrintedProduct;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class IdRangeParameterTest {

    @Test
    public void throwsWhenNull() {
        assertThrows(NullPointerException.class, () -> new IdRangeParameter(null));
    }

    @Test
    public void throwsWhenLowIsNegative() {
        assertThrows(IllegalArgumentException.class, () -> new IdRangeParameter(new Range<>(-1, 5)));
    }


    @Test
    public void testTest() {
        PrintedProduct book1 = new Book.Builder().id(1).wordNumber(2).name("fii").price(60).author("anyone").genre("fic").build();
        PrintedProduct book2 = new Book.Builder().id(5).wordNumber(2).name("fii").price(60).author("anyone").genre("fic").build();
        PrintedProduct book3 = new Book.Builder().id(10).wordNumber(2).name("fii").price(60).author("anyone").genre("fic").build();
        PrintedProduct book4 = new Book.Builder().id(15).wordNumber(2).name("fii").price(60).author("anyone").genre("fic").build();
        Parameter parameter = new IdRangeParameter(new Range<>(5, 12));
        assertFalse(parameter.test(book1));
        assertTrue(parameter.test(book2));
        assertTrue(parameter.test(book3));
        assertFalse(parameter.test(book4));
    }
}
