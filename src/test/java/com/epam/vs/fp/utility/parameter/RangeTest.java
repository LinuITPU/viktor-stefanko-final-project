package com.epam.vs.fp.utility.parameter;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class RangeTest {
    @Test
    public void throwsWhenArgumentsNull() {
        assertThrows(NullPointerException.class, () -> new Range<Integer>(null, null));
        assertThrows(NullPointerException.class, () -> new Range<>(1, null));
        assertThrows(NullPointerException.class, () -> new Range<>(null, 1));
    }

    @Test
    public void throwsWhenLowIsHigher() {
        assertThrows(IllegalArgumentException.class, () -> new Range<>(5, 2));
    }

    @Test
    public void testIsIn() {
        Range<Double> rangeDouble = new Range<>(-0.5, 10.5);
        Range<Integer> rangeInteger = new Range<>(-5, 10);
        assertTrue(rangeDouble.isIn(8.0));
        assertTrue(rangeDouble.isIn(-.03));
        assertTrue(rangeInteger.isIn(8));
        assertTrue(rangeInteger.isIn(-2));
        assertFalse(rangeDouble.isIn(80.0));
        assertFalse(rangeDouble.isIn(-3.5));
        assertFalse(rangeInteger.isIn(80));
        assertFalse(rangeInteger.isIn(-20));
    }
}
