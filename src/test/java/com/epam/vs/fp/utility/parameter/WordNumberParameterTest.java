package com.epam.vs.fp.utility.parameter;

import com.epam.vs.fp.beans.Book;
import com.epam.vs.fp.beans.Document;
import com.epam.vs.fp.beans.PrintedProduct;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class WordNumberParameterTest {

    @Test
    public void throwsWhenNegative() {
        assertThrows(IllegalArgumentException.class, () -> new WordNumberParameter(-1));
    }


    @Test
    public void testTest() {
        PrintedProduct document = new Document.Builder().id(1).wordNumber(2).name("fii").price(60).build();
        PrintedProduct book = new Book.Builder().id(1).wordNumber(2).name("fii").price(60).author("anyone").genre("fic").build();
        Parameter parameter = new WordNumberParameter(2);
        Parameter parameter_ = new WordNumberParameter(3);
        assertTrue(parameter.test(book));
        assertTrue(parameter.test(document));
        assertFalse(parameter_.test(book));
        assertFalse(parameter_.test(document));
    }
}
