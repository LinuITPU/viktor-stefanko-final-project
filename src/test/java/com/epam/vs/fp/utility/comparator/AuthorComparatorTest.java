package com.epam.vs.fp.utility.comparator;

import com.epam.vs.fp.beans.*;
import org.junit.Test;

import static org.junit.Assert.*;

public class AuthorComparatorTest {
    private static final PrintedProduct suitableProduct1 = new Book.Builder().author("Abc").genre("fi").id(2).wordNumber(300).price(400).name("any").build();
    private static final PrintedProduct suitableProduct2 = new Picture.Builder().author("Zara").genre("fi").id(2).colorState(Colorful.Color.BNW).price(400).name("any").build();
    private static final PrintedProduct unSuitableProduct = new Document.Builder().id(2).wordNumber(300).price(400).wordNumber(400).name("any").build();

    @Test
    public void testIsSuitable() {
        assertTrue(new AuthorComparator().isSuitable(suitableProduct1));
        assertTrue(new AuthorComparator().isSuitable(suitableProduct2));
        assertFalse(new AuthorComparator().isSuitable(unSuitableProduct));
    }

    @Test
    public void Compare() {
        hasAuthor p1 = (hasAuthor) suitableProduct1;
        hasAuthor p2 = (hasAuthor) suitableProduct2;
        assertEquals(new AuthorComparator().compare(p1, p2), CharSequence.compare(p1.getAuthor(), p2.getAuthor()));
        assertEquals(new AuthorComparator().compare(p2, p1), CharSequence.compare(p2.getAuthor(), p1.getAuthor()));
        assertEquals(new AuthorComparator().compare(p1, p1), 0);
    }
}
