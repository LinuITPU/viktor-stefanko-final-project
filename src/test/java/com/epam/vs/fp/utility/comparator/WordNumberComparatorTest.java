package com.epam.vs.fp.utility.comparator;

import com.epam.vs.fp.beans.*;
import org.junit.Test;

import static org.junit.Assert.*;

public class WordNumberComparatorTest {
    private static final PrintedProduct suitableProduct1 = new Book.Builder().author("Abc").genre("fi").id(2).wordNumber(1).price(400).name("any").build();
    private static final PrintedProduct suitableProduct2 = new Document.Builder().id(2).wordNumber(300).price(400).wordNumber(400).name("any").build();
    private static final PrintedProduct unSuitableProduct = new Picture.Builder().author("Zara").genre("fi").id(2).colorState(Colorful.Color.BNW).price(400).name("any").build();


    @Test
    public void testIsSuitable() {
        assertTrue(new WordNumberComparator().isSuitable(suitableProduct1));
        assertTrue(new WordNumberComparator().isSuitable(suitableProduct2));
        assertFalse(new WordNumberComparator().isSuitable(unSuitableProduct));
    }

    @Test
    public void Compare() {
        hasWords p1 = (hasWords) suitableProduct1;
        hasWords p2 = (hasWords) suitableProduct2;
        assertEquals(new WordNumberComparator().compare(p1, p2), Long.compare(p1.getWordNumber(), p2.getWordNumber()));
        assertEquals(new WordNumberComparator().compare(p2, p1), Long.compare(p2.getWordNumber(), p1.getWordNumber()));
        assertEquals(new WordNumberComparator().compare(p1, p1), 0);
    }
}
