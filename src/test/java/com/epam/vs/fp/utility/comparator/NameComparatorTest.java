package com.epam.vs.fp.utility.comparator;

import com.epam.vs.fp.beans.Book;
import com.epam.vs.fp.beans.Colorful;
import com.epam.vs.fp.beans.Picture;
import com.epam.vs.fp.beans.PrintedProduct;
import org.junit.Test;

import static org.junit.Assert.*;

public class NameComparatorTest {
    private static final PrintedProduct p2 = new Book.Builder().author("Abc").genre("AAA").id(1).wordNumber(300).price(400).name("Any").build();
    private static final PrintedProduct p1 = new Picture.Builder().author("Abc").genre("AAA").id(1).colorState(Colorful.Color.BNW).price(400).name("Zany").build();

    @Test
    public void testIsSuitable() {
        assertTrue(new NameComparator().isSuitable(p1));
        assertFalse(new NameComparator().isSuitable(""));
    }

    @Test
    public void Compare() {
        assertEquals(new NameComparator().compare(p1, p2), CharSequence.compare(p1.getName(), p2.getName()));
        assertEquals(new NameComparator().compare(p2, p1), CharSequence.compare(p2.getName(), p1.getName()));
        assertEquals(new NameComparator().compare(p1, p1), 0);
    }
}
