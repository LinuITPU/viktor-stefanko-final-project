package com.epam.vs.fp.utility.comparator;

import com.epam.vs.fp.beans.*;
import org.junit.Test;

import static org.junit.Assert.*;

public class TypeComparatorTest {
    private static final PrintedProduct p2 = new Book.Builder().author("Abc").genre("AAA").id(1).wordNumber(300).price(400).name("Any").build();
    private static final PrintedProduct p1 = new Picture.Builder().author("Abc").genre("AAA").id(1).colorState(Colorful.Color.BNW).price(400).name("Zany").build();
    private static final PrintedProduct p3 = new Document.Builder().id(1).price(400).name("Any").wordNumber(1000).build();

    @Test
    public void testIsSuitable() {
        assertTrue(new TypeComparator().isSuitable(p1));
        assertTrue(new TypeComparator().isSuitable(p2));
        assertTrue(new TypeComparator().isSuitable(p3));
        assertFalse(new TypeComparator().isSuitable(""));
    }

    @Test
    public void Compare() {
        assertEquals(new TypeComparator().compare(p1, p2), CharSequence.compare(p1.getClass().getSimpleName(), p2.getClass().getSimpleName()));
        assertEquals(new TypeComparator().compare(p2, p1), CharSequence.compare(p2.getClass().getSimpleName(), p1.getClass().getSimpleName()));
        assertEquals(new TypeComparator().compare(p1, p1), 0);
    }
}
