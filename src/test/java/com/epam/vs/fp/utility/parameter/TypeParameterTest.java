package com.epam.vs.fp.utility.parameter;

import com.epam.vs.fp.beans.*;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TypeParameterTest {

    @Test
    public void throwsWhenNullOrEmpty() {
        assertThrows(NullPointerException.class, () -> new TypeParameter(null));
        assertThrows(NullPointerException.class, () -> new TypeParameter(""));
        assertThrows(NullPointerException.class, () -> new TypeParameter("  "));
    }

    @Test
    public void testTest() {
        PrintedProduct document = new Document.Builder().id(1).wordNumber(2).name("fii").price(60).build();
        PrintedProduct book = new Book.Builder().id(1).wordNumber(2).name("fii").price(60).author("anyone").genre("fic").build();
        PrintedProduct picture = new Picture.Builder().id(1).colorState(Colorful.Color.BNW).name("fii").price(60).author("anyone").genre("fic").build();
        Parameter dParameter = new TypeParameter("Document");
        Parameter bParameter = new TypeParameter("Book");
        Parameter pParameter = new TypeParameter("Picture");

        assertTrue(dParameter.test(document));
        assertTrue(bParameter.test(book));
        assertTrue(pParameter.test(picture));

        assertFalse(dParameter.test(book));
        assertFalse(bParameter.test(picture));
        assertFalse(pParameter.test(document));

        assertFalse(dParameter.test(picture));
        assertFalse(bParameter.test(document));
        assertFalse(pParameter.test(book));
    }
}
