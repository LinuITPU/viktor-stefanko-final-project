package com.epam.vs.fp.utility.parameter;

import com.epam.vs.fp.beans.Book;
import com.epam.vs.fp.beans.PrintedProduct;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PriceParameterTest {

    @Test
    public void throwsWhenNegative() {
        assertThrows(IllegalArgumentException.class, () -> new PriceParameter(-1));
    }


    @Test
    public void testTestIntegers() {
        PrintedProduct book = new Book.Builder().id(1).wordNumber(2).name("fii").price(60).author("anyone").genre("fic").build();
        Parameter parameter = new PriceParameter(60);
        Parameter parameter_ = new PriceParameter(5);
        assertTrue(parameter.test(book));
        assertFalse(parameter_.test(book));
    }

    @Test
    public void testTestDoubles() {
        PrintedProduct book = new Book.Builder().id(1).wordNumber(2).name("fii").price(60.51).author("anyone").genre("fic").build();
        Parameter parameter = new PriceParameter(60.51);
        Parameter parameter_ = new PriceParameter(5.1);
        assertTrue(parameter.test(book));
        assertFalse(parameter_.test(book));
    }
}
