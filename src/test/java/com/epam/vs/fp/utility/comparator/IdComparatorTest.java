package com.epam.vs.fp.utility.comparator;

import com.epam.vs.fp.beans.*;
import org.junit.Test;

import static org.junit.Assert.*;

public class IdComparatorTest {
    private static final PrintedProduct p2 = new Book.Builder().author("Abc").genre("AAA").id(1).wordNumber(300).price(400).name("Any").build();
    private static final PrintedProduct p1 = new Picture.Builder().author("Abc").genre("AAA").id(20).colorState(Colorful.Color.BNW).price(400).name("Zany").build();

    @Test
    public void testIsSuitable() {
        assertTrue(new IdComparator().isSuitable(p1));
        assertFalse(new IdComparator().isSuitable(""));
    }

    @Test
    public void Compare() {
        assertEquals(new IdComparator().compare(p1, p2), Integer.compare(p1.getId(), p2.getId()));
        assertEquals(new IdComparator().compare(p2, p1), Integer.compare(p2.getId(), p1.getId()));
        assertEquals(new IdComparator().compare(p1, p1), 0);
    }
}
