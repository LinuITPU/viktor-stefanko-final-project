package com.epam.vs.fp.utility.parameter;

import com.epam.vs.fp.beans.*;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class NameParameterTest {

    @Test
    public void throwsWhenNullOrEmpty() {
        assertThrows(NullPointerException.class, () -> new NameParameter(null));
        assertThrows(NullPointerException.class, () -> new NameParameter(""));
        assertThrows(NullPointerException.class, () -> new NameParameter("  "));
    }

    @Test
    public void testTest() {
        PrintedProduct book = new Book.Builder().id(1).wordNumber(2).name("fii").price(60).author("anyone").genre("fic").build();
        Parameter parameter = new NameParameter("fii");
        Parameter parameter_ = new NameParameter("foo");
        assertTrue(parameter.test(book));
        assertFalse(parameter_.test(book));
    }
}
