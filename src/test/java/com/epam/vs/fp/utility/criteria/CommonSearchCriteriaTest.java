package com.epam.vs.fp.utility.criteria;

import com.epam.vs.fp.beans.Book;
import com.epam.vs.fp.beans.PrintedProduct;
import com.epam.vs.fp.utility.parameter.AuthorParameter;
import com.epam.vs.fp.utility.parameter.IdRangeParameter;
import com.epam.vs.fp.utility.parameter.PriceParameter;
import com.epam.vs.fp.utility.parameter.Range;
import org.junit.Test;
import static org.junit.Assert.*;

public class CommonSearchCriteriaTest {
    @Test
    public void filters() {
        SearchCriteria<PrintedProduct> criteria = new CommonSearchCriteria();
        criteria.add(new AuthorParameter("Me"));
        criteria.add(new IdRangeParameter(new Range<>(3, 65)));
        criteria.add(new PriceParameter(600));

        PrintedProduct goodProduct = new Book.Builder().id(50).author("Me").wordNumber(13).price(600).name("Any").genre("fiction").build();
        assertTrue(criteria.test(goodProduct));
        PrintedProduct badProduct1 = new Book.Builder().id(70).author("Me").wordNumber(13).price(600).name("Any").genre("fiction").build();
        PrintedProduct badProduct2 = new Book.Builder().id(65).author("MeO").wordNumber(13).price(600).name("Any").genre("fiction").build();
        PrintedProduct badProduct3 = new Book.Builder().id(65).author("Me").wordNumber(13).price(1).name("Any").genre("fiction").build();
        assertFalse(criteria.test(badProduct1));
        assertFalse(criteria.test(badProduct2));
        assertFalse(criteria.test(badProduct3));
    }
}
