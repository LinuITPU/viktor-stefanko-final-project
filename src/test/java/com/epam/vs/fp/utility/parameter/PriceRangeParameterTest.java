package com.epam.vs.fp.utility.parameter;

import com.epam.vs.fp.beans.Book;
import com.epam.vs.fp.beans.PrintedProduct;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PriceRangeParameterTest {

    @Test
    public void throwsWhenNull() {
        assertThrows(NullPointerException.class, () -> new PriceRangeParameter(null));
    }

    @Test
    public void throwsWhenLowIsNegative() {
        assertThrows(IllegalArgumentException.class, () -> new PriceRangeParameter(new Range<>(-1.0, 5.0)));
    }


    @Test
    public void testTest() {
        PrintedProduct book1 = new Book.Builder().id(1).wordNumber(2).name("fii").price(0).author("anyone").genre("fic").build();
        PrintedProduct book2 = new Book.Builder().id(1).wordNumber(2).name("fii").price(10).author("anyone").genre("fic").build();
        PrintedProduct book3 = new Book.Builder().id(1).wordNumber(2).name("fii").price(20).author("anyone").genre("fic").build();
        PrintedProduct book4 = new Book.Builder().id(1).wordNumber(2).name("fii").price(40).author("anyone").genre("fic").build();
        Parameter parameter = new PriceRangeParameter(new Range<>(8.0, 25.0));
        assertFalse(parameter.test(book1));
        assertTrue(parameter.test(book2));
        assertTrue(parameter.test(book3));
        assertFalse(parameter.test(book4));
    }
}
