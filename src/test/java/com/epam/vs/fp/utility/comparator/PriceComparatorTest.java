package com.epam.vs.fp.utility.comparator;

import com.epam.vs.fp.beans.Book;
import com.epam.vs.fp.beans.Colorful;
import com.epam.vs.fp.beans.Picture;
import com.epam.vs.fp.beans.PrintedProduct;
import org.junit.Test;

import static org.junit.Assert.*;

public class PriceComparatorTest {
    private static final PrintedProduct p2 = new Book.Builder().author("Abc").genre("AAA").id(1).wordNumber(300).price(111).name("Any").build();
    private static final PrintedProduct p1 = new Picture.Builder().author("Abc").genre("AAA").id(1).colorState(Colorful.Color.BNW).price(999).name("Any").build();

    @Test
    public void testIsSuitable() {
        assertTrue(new PriceComparator().isSuitable(p1));
        assertFalse(new PriceComparator().isSuitable(""));
    }

    @Test
    public void Compare() {
        assertEquals(new PriceComparator().compare(p1, p2), Double.compare(p1.getPrice(), p2.getPrice()));
        assertEquals(new PriceComparator().compare(p2, p1), Double.compare(p2.getPrice(), p1.getPrice()));
        assertEquals(new PriceComparator().compare(p1, p1), 0);
    }
}
