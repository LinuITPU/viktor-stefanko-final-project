package com.epam.vs.fp.utility.parameter;

import com.epam.vs.fp.beans.*;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class IdParameterTest {

    @Test
    public void throwsWhenNegative() {
        assertThrows(IllegalArgumentException.class, () -> new IdParameter(-1));
    }


    @Test
    public void testTest() {
        PrintedProduct book = new Book.Builder().id(1).wordNumber(2).name("fii").price(60).author("anyone").genre("fic").build();
        Parameter parameter = new IdParameter(1);
        Parameter parameter_ = new IdParameter(5);
        assertTrue(parameter.test(book));
        assertFalse(parameter_.test(book));
    }
}
