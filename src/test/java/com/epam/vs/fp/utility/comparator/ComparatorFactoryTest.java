package com.epam.vs.fp.utility.comparator;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class ComparatorFactoryTest {
    @Test
    public void testClasses () {
        assertEquals(ComparatorFactory.ID.getComparator().getClass(), IdComparator.class);
        assertEquals(ComparatorFactory.AUTHOR.getComparator().getClass(), AuthorComparator.class);
        assertEquals(ComparatorFactory.COLOR.getComparator().getClass(), ColorComparator.class);
        assertEquals(ComparatorFactory.NAME.getComparator().getClass(), NameComparator.class);
        assertEquals(ComparatorFactory.GENRE.getComparator().getClass(), GenreComparator.class);
        assertEquals(ComparatorFactory.PRICE.getComparator().getClass(), PriceComparator.class);
        assertEquals(ComparatorFactory.TYPE.getComparator().getClass(), TypeComparator.class);
        assertEquals(ComparatorFactory.WORD_NUMBER.getComparator().getClass(), WordNumberComparator.class);
    }
}
