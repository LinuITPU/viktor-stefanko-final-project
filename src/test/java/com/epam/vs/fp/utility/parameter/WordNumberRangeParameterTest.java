package com.epam.vs.fp.utility.parameter;

import com.epam.vs.fp.beans.Book;
import com.epam.vs.fp.beans.PrintedProduct;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class WordNumberRangeParameterTest {

    @Test
    public void throwsWhenNull() {
        assertThrows(NullPointerException.class, () -> new WordNumberRangeParameter(null));
    }

    @Test
    public void throwsWhenLowIsNegative() {
        assertThrows(IllegalArgumentException.class, () -> new WordNumberRangeParameter(new Range<>(-1L, 5L)));
    }


    @Test
    public void testTest() {
        PrintedProduct book1 = new Book.Builder().id(1).wordNumber(0).name("fii").price(60).author("anyone").genre("fic").build();
        PrintedProduct book2 = new Book.Builder().id(1).wordNumber(10).name("fii").price(60).author("anyone").genre("fic").build();
        PrintedProduct book3 = new Book.Builder().id(1).wordNumber(20).name("fii").price(60).author("anyone").genre("fic").build();
        PrintedProduct book4 = new Book.Builder().id(1).wordNumber(30).name("fii").price(60).author("anyone").genre("fic").build();
        Parameter parameter = new WordNumberRangeParameter(new Range<>(5L, 25L));
        assertFalse(parameter.test(book1));
        assertTrue(parameter.test(book2));
        assertTrue(parameter.test(book3));
        assertFalse(parameter.test(book4));
    }
}
