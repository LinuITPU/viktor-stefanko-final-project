package com.epam.vs.fp.utility.comparator;

import com.epam.vs.fp.beans.*;
import org.junit.Test;

import static org.junit.Assert.*;

public class ColorComparatorTest {
    private static final PrintedProduct suitableProduct1 = new Picture.Builder().author("Abc").genre("fi").id(2).price(400).name("any").colorState(Colorful.Color.COLORFUL).build();
    private static final PrintedProduct suitableProduct2 = new Picture.Builder().author("Abc").genre("fi").id(2).colorState(Colorful.Color.BNW).price(400).name("any").build();
    private static final PrintedProduct unSuitableProduct1 = new Document.Builder().id(2).wordNumber(300).price(400).wordNumber(400).name("any").build();
    private static final PrintedProduct unSuitableProduct2 = new Book.Builder().author("Abc").genre("fi").id(2).wordNumber(300).price(400).name("any").build();


    @Test
    public void testIsSuitable() {
        assertTrue(new ColorComparator().isSuitable(suitableProduct1));
        assertFalse(new ColorComparator().isSuitable(unSuitableProduct1));
        assertFalse(new ColorComparator().isSuitable(unSuitableProduct2));
    }

    @Test
    public void Compare() {
        Colorful p1 = (Colorful) suitableProduct1;
        Colorful p2 = (Colorful) suitableProduct2;
        assertEquals(new ColorComparator().compare(p1, p2), Integer.compare(p1.getColorState().getValue(), p2.getColorState().getValue()));
        assertEquals(new ColorComparator().compare(p2, p1), Integer.compare(p2.getColorState().getValue(), p1.getColorState().getValue()));
        assertEquals(new ColorComparator().compare(p1, p1), 0);
    }
}
