package com.epam.vs.fp.controller.converter;

import com.epam.vs.fp.beans.Colorful;
import com.epam.vs.fp.utility.parameter.ColorParameter;
import com.epam.vs.fp.utility.parameter.Parameter;

public class ColorParameterConverter extends AbstractParameterConverter{
    public ColorParameterConverter() {
        super("color");
    }
    @Override
    protected Parameter internalConvert(String request) throws ParameterConversionException{
        if (request == null || request.isBlank()) {throw new ParameterConversionException(this.parameterName()+" cannot be nothing");}
        if (!(request.equalsIgnoreCase("bnw") || request.equalsIgnoreCase("colorful") || request.equalsIgnoreCase("colourful")))
        {
            throw new ParameterConversionException("Such color does not exist in the system");
        }
        return new ColorParameter(
                request.equalsIgnoreCase("bnw") ? Colorful.Color.BNW : Colorful.Color.COLORFUL
        );
    }
}
