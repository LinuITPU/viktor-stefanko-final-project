package com.epam.vs.fp.controller.converter;

import com.epam.vs.fp.utility.parameter.IdParameter;
import com.epam.vs.fp.utility.parameter.Parameter;

public class IdParameterConverter extends AbstractParameterConverter{
    public IdParameterConverter() {
        super("id");
    }
    @Override
    protected Parameter internalConvert(String request) throws ParameterConversionException {
        if (request == null || request.isBlank()) {throw new ParameterConversionException(this.parameterName()+" cannot be nothing");}

        int id;
        try {
            id = Integer.parseInt(request);
        } catch (NumberFormatException e) {
            throw new ParameterConversionException(parameterName + " must be a whole number");
        }
        if (id < 0) {throw new ParameterConversionException("id cannot be negative");}
        return new IdParameter(id);
    }
}
