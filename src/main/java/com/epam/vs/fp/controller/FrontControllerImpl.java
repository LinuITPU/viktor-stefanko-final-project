package com.epam.vs.fp.controller;

import com.epam.vs.fp.beans.PrintedProduct;
import com.epam.vs.fp.view.ViewPresenter;
import com.epam.vs.fp.view.WarehousePresenter;
import java.util.*;

public class FrontControllerImpl implements FrontController{
    private static final FrontController INSTANCE = new FrontControllerImpl();
    private static final ViewPresenter<PrintedProduct> PRESENTER = WarehousePresenter.getINSTANCE();

    private static final HashMap<String, SmallController> CONTROLLERS = new HashMap<>();
    static {
        CONTROLLERS.put("search", FilterController.getINSTANCE());
        CONTROLLERS.put("browse", BrowseController.getINSTANCE());
        CONTROLLERS.put("help", HelpController.getINSTANCE());
    }

    private FrontControllerImpl(){}

    @Override
    public boolean listen(String strCommand) {
        if (strCommand == null || strCommand.isBlank()){return true;}
        Queue<String> commandQueue = new LinkedList<>();
        for (String commandPart: strCommand.split(" ")) {
            commandQueue.offer(commandPart);
        }
        String command = commandQueue.poll();
        command = command.toLowerCase();
        switch (command) {
            case "exit": {
                if (commandQueue.peek() != null) {
                    PRESENTER.presentException(new InputMismatchException("the command does not take arguments"));
                    return true;
                }
                return false;
            }
            case "sort": {
                try {
                    BrowseController.setSortingAlgorithm(commandQueue);
                } catch (InputMismatchException e) {
                    PRESENTER.presentException(e);
                    return true;
                }
                command = "browse"; // to display the table after setting the sorting algorithm for it
                break;
            }
        }


        SmallController neededController = CONTROLLERS.get(command);
        if (neededController == null) {
            PRESENTER.presentException(new InputMismatchException("command not found"));
            return true;
        }
        Optional<List<? extends PrintedProduct>> result;
        try {
            result = neededController.execute(commandQueue);
        } catch (InputMismatchException e) {
            PRESENTER.presentException(e);
            return true;
        }
        if (result.isEmpty()) {return true;} // the command does not suppose a table output
        else {
            PRESENTER.outputEntries(
                    neededController instanceof BrowseController ? result.get()
                            : ((BrowseController)(CONTROLLERS.get("browse"))).sort(result.get()),
                    BrowseController.getSortingAlgorithm(), BrowseController.isAscending()
            );
        }
        return true;
    }

    public static FrontController getINSTANCE() {
        return INSTANCE;
    }
}
