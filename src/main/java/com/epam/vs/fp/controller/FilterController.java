package com.epam.vs.fp.controller;

import com.epam.vs.fp.beans.PrintedProduct;
import com.epam.vs.fp.service.ProductService;
import com.epam.vs.fp.utility.criteria.CommonSearchCriteria;
import com.epam.vs.fp.utility.criteria.SearchCriteria;
import com.epam.vs.fp.controller.converter.RawConverters;
import com.epam.vs.fp.service.ServiceFactory;
import java.util.*;

class FilterController implements SmallController {
    private final static FilterController INSTANCE = new FilterController();
    private static final ProductService SERVICE = ServiceFactory.INSTANCE.getProductService();
    private FilterController(){}
    private static final Map<String, RawConverters> paramConverters = new HashMap<>();
    static {
        paramConverters.put("price", RawConverters.PRICE);
        paramConverters.put("price range", RawConverters.PRICE_RANGE);
        paramConverters.put("author", RawConverters.AUTHOR);
        paramConverters.put("color", RawConverters.COLOR);
        paramConverters.put("colour", RawConverters.COLOR);
        paramConverters.put("genre", RawConverters.GENRE);
        paramConverters.put("id", RawConverters.ID);
        paramConverters.put("id range", RawConverters.ID_RANGE);
        paramConverters.put("name", RawConverters.NAME);
        paramConverters.put("word-number", RawConverters.WORD_NUMBER);
        paramConverters.put("word-number range", RawConverters.WORD_NUMBER_RANGE);
        paramConverters.put("type", RawConverters.TYPE);
    }

    public static FilterController getINSTANCE() {
        return INSTANCE;
    }

    @Override
    public Optional<List<? extends PrintedProduct>> execute(Queue<String> commandQueue) throws InputMismatchException{
        if (commandQueue.peek() == null) {
            throw new InputMismatchException("'search' cannot be used without arguments");
        }
        SearchCriteria<? extends PrintedProduct> searchCriteria;
        searchCriteria = new CommonSearchCriteria();
        while (true) {
            String currentParameter = commandQueue.poll();
            if (currentParameter == null) {break;}
            currentParameter = currentParameter.toLowerCase();

            RawConverters rawConverter;
            if ("range".equalsIgnoreCase(commandQueue.peek())) { // create a range parameter then
                rawConverter = paramConverters.get(currentParameter + " range");
            } else {
                rawConverter = paramConverters.get(currentParameter);
            }
            if (rawConverter == null) { // user entered a wrong parameter or a range where not possible
                if (paramConverters.get(currentParameter) != null) { // the property is there, but a range cannot be applied to id
                    throw new InputMismatchException("range for property '"+currentParameter+"' is not possible");
                } else { // the user misspelled the property
                    throw new InputMismatchException("property '"+currentParameter+"' does not exist");
                }
            }
            String converterRequest;
            if ("range".equalsIgnoreCase(commandQueue.peek())) { // create a range parameter then
                commandQueue.poll();
                String currentParameterValues;
                try {
                    currentParameterValues = commandQueue.poll() + " " + commandQueue.poll();
                } catch (NullPointerException e) {
                    throw new InputMismatchException("property '"+currentParameter+"' misses 1 or 2 of its range values");
                }
                converterRequest = currentParameterValues;
            } else { // not ranged parameter
                String currentParameterValue = commandQueue.poll();
                if (currentParameterValue == null) {throw new InputMismatchException("property '"+currentParameter+"' misses the value");}
                converterRequest = currentParameterValue;
            }
            try {
                searchCriteria.add(rawConverter.getConverter().convert(converterRequest));
            } catch (IllegalArgumentException e) {
                throw new InputMismatchException(e.getMessage());
            }
        }
        return Optional.of(new LinkedList<>(SERVICE.filter(SERVICE.getDB(), (searchCriteria))));
    }
}
