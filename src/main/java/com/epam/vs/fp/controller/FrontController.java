package com.epam.vs.fp.controller;

public interface FrontController {
    boolean listen(String strCommand);
}
