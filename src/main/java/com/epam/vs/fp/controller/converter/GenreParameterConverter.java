package com.epam.vs.fp.controller.converter;

import com.epam.vs.fp.utility.parameter.GenreParameter;
import com.epam.vs.fp.utility.parameter.Parameter;

public class GenreParameterConverter extends AbstractParameterConverter{
    public GenreParameterConverter() {
        super("genre");
    }
    @Override
    protected Parameter internalConvert(String request) throws ParameterConversionException{
        if (request == null || request.isBlank()) {throw new ParameterConversionException(this.parameterName()+" cannot be nothing");}
        return new GenreParameter(request);
    }
}
