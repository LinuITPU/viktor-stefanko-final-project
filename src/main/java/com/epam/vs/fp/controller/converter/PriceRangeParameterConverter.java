package com.epam.vs.fp.controller.converter;

import com.epam.vs.fp.utility.parameter.*;

public class PriceRangeParameterConverter extends AbstractParameterConverter{
    public PriceRangeParameterConverter() {
        super("price range");
    }
    @Override
    protected Parameter internalConvert(String request) throws ParameterConversionException {
        if (request == null || request.isBlank()) {throw new ParameterConversionException(this.parameterName()+" cannot be nothing");}
        String[] rangeBoundaries = request.split(" ");
        if (rangeBoundaries.length != 2) {throw new ParameterConversionException(this.parameterName() + " should have exactly 2 numbers");}

        double low;
        double high;
        try {
            low = Long.parseLong(rangeBoundaries[0]);
            high = Long.parseLong(rangeBoundaries[1]);
        } catch (NumberFormatException e) {
            throw new ParameterConversionException(parameterName + " values must be whole numbers");
        }
        if (low < 0) {throw new ParameterConversionException("price must be non-negative");}
        return new PriceRangeParameter(new Range<>(low, high));
    }
}
