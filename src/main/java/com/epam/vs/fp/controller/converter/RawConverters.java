package com.epam.vs.fp.controller.converter;

public enum RawConverters {
    AUTHOR(new AuthorParameterConverter()),
    COLOR(new ColorParameterConverter()),
    GENRE(new GenreParameterConverter()),
    ID(new IdParameterConverter()),
    ID_RANGE(new IdRangeParameterConverter()),
    NAME(new NameParameterConverter()),
    PRICE(new PriceParameterConverter()),
    PRICE_RANGE(new PriceRangeParameterConverter()),
    WORD_NUMBER(new WordNumberParameterConverter()),
    WORD_NUMBER_RANGE(new WordNumberRangeParameterConverter()),
    TYPE(new TypeParameterConverter());

    private final ParameterConverter converter;
    RawConverters(ParameterConverter converter) {
        this.converter = converter;
    }

    public ParameterConverter getConverter() {
        return converter;
    }
}
