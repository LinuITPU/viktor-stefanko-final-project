package com.epam.vs.fp.controller.converter;

import com.epam.vs.fp.utility.parameter.*;

public class IdRangeParameterConverter extends AbstractParameterConverter{
    public IdRangeParameterConverter() {
        super("id range");
    }
    @Override
    protected Parameter internalConvert(String request) throws ParameterConversionException {
        if (request == null || request.isBlank()) {throw new ParameterConversionException(this.parameterName()+" cannot be nothing");}

        String[] rangeBoundaries = request.split(" ");
        if (rangeBoundaries.length != 2) {throw new ParameterConversionException(this.parameterName() + " should have exactly 2 numbers");}

        int low;
        int high;
        try {
            low = Integer.parseInt(rangeBoundaries[0]);
            high = Integer.parseInt(rangeBoundaries[1]);
        } catch (NumberFormatException e) {
            throw new ParameterConversionException(parameterName + " values must be whole numbers");
        }
        if (low < 0) {throw new ParameterConversionException("id cannot be negative");}
        return new IdRangeParameter(new Range<>(low, high));
    }
}
