package com.epam.vs.fp.controller.converter;

import com.epam.vs.fp.utility.parameter.NameParameter;
import com.epam.vs.fp.utility.parameter.Parameter;

public class NameParameterConverter extends AbstractParameterConverter{
    public NameParameterConverter() {
        super("name");
    }
    @Override
    protected Parameter internalConvert(String request) throws ParameterConversionException {
        if (request == null || request.isBlank()) {throw new ParameterConversionException(this.parameterName()+" cannot be nothing");}
        return new NameParameter(request);
    }
}
