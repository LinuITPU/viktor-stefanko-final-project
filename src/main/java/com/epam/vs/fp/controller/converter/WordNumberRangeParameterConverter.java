package com.epam.vs.fp.controller.converter;

import com.epam.vs.fp.utility.parameter.Parameter;
import com.epam.vs.fp.utility.parameter.Range;
import com.epam.vs.fp.utility.parameter.WordNumberRangeParameter;

public class WordNumberRangeParameterConverter extends AbstractParameterConverter{
    public WordNumberRangeParameterConverter() {
        super("word-number range");
    }
    @Override
    protected Parameter internalConvert(String request) throws ParameterConversionException {
        if (request == null || request.isBlank()) {throw new ParameterConversionException(this.parameterName()+" cannot be nothing");}
        String[] rangeBoundaries = request.split(" ");
        if (rangeBoundaries.length != 2) {throw new ParameterConversionException(this.parameterName() + " should have exactly 2 numbers");}

        long low;
        long high;
        try {
            low = Long.parseLong(rangeBoundaries[0]);
            high = Long.parseLong(rangeBoundaries[1]);
        } catch (NumberFormatException e) {
            throw new ParameterConversionException(parameterName + " values must be whole numbers");
        }
        if (low < 1) {throw new ParameterConversionException("word-number must be non-negative");}
        return new WordNumberRangeParameter(new Range<>(low, high));
    }
}
