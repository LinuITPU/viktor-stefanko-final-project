package com.epam.vs.fp.controller;

import com.epam.vs.fp.beans.PrintedProduct;
import com.epam.vs.fp.view.ViewPresenter;
import com.epam.vs.fp.view.WarehousePresenter;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Optional;
import java.util.Queue;

class HelpController implements SmallController {

    private static final HelpController INSTANCE = new HelpController();
    private static final ViewPresenter<PrintedProduct> PRESENTER = WarehousePresenter.getINSTANCE();

    public static HelpController getINSTANCE() {
        return INSTANCE;
    }

    private HelpController(){}


    @Override
    public Optional<List<? extends PrintedProduct>> execute(Queue<String> commandQueue) throws InputMismatchException {
        String command = commandQueue.poll();
        if (command == null) { // the user entered 'help' without any additional arguments
            PRESENTER.printHelp();
        } else {
            if (commandQueue.peek() != null) {
                throw new InputMismatchException("redundant arguments found");
            }
            PRESENTER.printHelp(command);
        }
        return Optional.empty();
    }
}
