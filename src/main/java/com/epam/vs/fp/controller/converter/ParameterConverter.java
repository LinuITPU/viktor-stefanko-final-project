package com.epam.vs.fp.controller.converter;

import com.epam.vs.fp.utility.parameter.Parameter;

public interface ParameterConverter {
    Parameter convert(String request) throws ParameterConversionException;
    String parameterName();
}
