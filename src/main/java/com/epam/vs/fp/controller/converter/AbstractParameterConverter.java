package com.epam.vs.fp.controller.converter;

import com.epam.vs.fp.utility.parameter.Parameter;

public abstract class AbstractParameterConverter implements ParameterConverter {
    final String parameterName;
    AbstractParameterConverter(String parameterName) {
        if (parameterName == null || parameterName.isBlank()){throw new IllegalArgumentException();}
        this.parameterName = parameterName;
    }

    @Override
    public Parameter convert(String request) throws ParameterConversionException{
        return internalConvert(request);
    }

    @Override
    public String parameterName() {
        return parameterName;
    }

    protected abstract Parameter internalConvert(String request) throws ParameterConversionException;
}
