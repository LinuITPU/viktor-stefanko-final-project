package com.epam.vs.fp.controller;

import com.epam.vs.fp.beans.PrintedProduct;
import com.epam.vs.fp.service.ProductService;
import com.epam.vs.fp.service.ServiceFactory;
import com.epam.vs.fp.view.column.ColumnFactory;
import com.epam.vs.fp.view.column.Column;
import java.util.*;

class BrowseController implements SmallController {
    private static final ProductService SERVICE = ServiceFactory.INSTANCE.getProductService();

    private static final Map<String, Column> COMPARATORS = new HashMap<>();
    static {
        COMPARATORS.put("author", ColumnFactory.AUTHOR.getColumn());
        COMPARATORS.put("color", ColumnFactory.COLOR.getColumn());
        COMPARATORS.put("colour", ColumnFactory.COLOR.getColumn());
        COMPARATORS.put("genre", ColumnFactory.GENRE.getColumn());
        COMPARATORS.put("id", ColumnFactory.ID.getColumn());
        COMPARATORS.put("name", ColumnFactory.NAME.getColumn());
        COMPARATORS.put("price", ColumnFactory.PRICE.getColumn());
        COMPARATORS.put("word-number", ColumnFactory.WORD_LENGTH.getColumn());
        COMPARATORS.put("type", ColumnFactory.TYPE.getColumn());
    }

    private static Column sortingAlgorithm = ColumnFactory.ID.getColumn(); // default value
    private static boolean isAscending = true;
    private static final BrowseController INSTANCE = new BrowseController();

    public static BrowseController getINSTANCE() {
        return INSTANCE;
    }

    private BrowseController(){}

    @Override
    public Optional<List<? extends PrintedProduct>> execute(Queue<String> commandQueue) {
        if (commandQueue.peek() != null) {
            throw new InputMismatchException("the command does not take arguments");
        }
        List<? extends PrintedProduct> db = SERVICE.getDB();
        return Optional.of(sort(db));
    }

    public List<? extends PrintedProduct> sort(List<? extends PrintedProduct> products) {
        List<? extends PrintedProduct> sorted = SERVICE.sort(products, sortingAlgorithm.getColumnComparatorFactory());
        return isAscending ? sorted : SERVICE.reverse(sorted);
    }

    static void setSortingAlgorithm(Queue<String> commandQueue) throws InputMismatchException{
        String newAlgorithm = commandQueue.poll();
        if (newAlgorithm == null || newAlgorithm.isBlank()) {throw new InputMismatchException("sort property must be entered following the 'sort' command");}
        newAlgorithm = newAlgorithm.toLowerCase();
        BrowseController.sortingAlgorithm = COMPARATORS.get(newAlgorithm);
        if (sortingAlgorithm == null) {
            throw new InputMismatchException("property '"+newAlgorithm+"' does not exist");
        }
        String ascending = commandQueue.poll();
        if (ascending == null || ascending.equalsIgnoreCase("ascending") || ascending.equalsIgnoreCase("asc")) {
            BrowseController.isAscending = true;
        } else if (ascending.equalsIgnoreCase("descending") || ascending.equalsIgnoreCase("desc")) {
            BrowseController.isAscending = false;
        } else {throw new InputMismatchException("unsupported sorting order: '"+ascending.toLowerCase()+"'");}
        if (commandQueue.poll() != null) {
            throw new InputMismatchException("redundant arguments found");
        }
    }

    public static Column getSortingAlgorithm() {
        return sortingAlgorithm;
    }
    public static boolean isAscending() {
        return isAscending;
    }
}
