package com.epam.vs.fp.controller.converter;

import com.epam.vs.fp.utility.parameter.Parameter;
import com.epam.vs.fp.utility.parameter.PriceParameter;

public class PriceParameterConverter extends AbstractParameterConverter{
    public PriceParameterConverter() {
        super("price");
    }
    @Override
    protected Parameter internalConvert(String request) throws ParameterConversionException {
        if (request == null || request.isBlank()) {throw new ParameterConversionException(this.parameterName()+" cannot be nothing");}

        double price;
        try {
            price = Double.parseDouble(request);
        } catch (NumberFormatException e) {
            throw new ParameterConversionException(parameterName + " must be a number");
        }
        if (price < 0) {throw new ParameterConversionException("price cannot be negative");}
        return new PriceParameter(price);
    }
}
