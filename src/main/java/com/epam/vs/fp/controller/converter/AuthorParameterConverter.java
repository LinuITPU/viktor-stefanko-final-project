package com.epam.vs.fp.controller.converter;

import com.epam.vs.fp.utility.parameter.AuthorParameter;
import com.epam.vs.fp.utility.parameter.Parameter;

public class AuthorParameterConverter extends AbstractParameterConverter{
    public AuthorParameterConverter() {
        super("author");
    }
    @Override
    protected Parameter internalConvert(String request) throws ParameterConversionException{
        if (request == null || request.isBlank()) {throw new ParameterConversionException(this.parameterName()+" cannot be nothing");}
        return new AuthorParameter(request);
    }
}
