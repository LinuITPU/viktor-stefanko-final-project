package com.epam.vs.fp.controller.converter;

import com.epam.vs.fp.utility.parameter.Parameter;
import com.epam.vs.fp.utility.parameter.TypeParameter;

public class TypeParameterConverter extends AbstractParameterConverter{
    public TypeParameterConverter() {
        super("type");
    }
    @Override
    protected Parameter internalConvert(String request) {
        if (request == null || request.isBlank()) {throw new ParameterConversionException(this.parameterName()+" cannot be nothing");}
        return new TypeParameter(request);
    }
}
