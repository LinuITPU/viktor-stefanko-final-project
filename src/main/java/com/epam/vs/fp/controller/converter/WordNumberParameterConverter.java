package com.epam.vs.fp.controller.converter;

import com.epam.vs.fp.utility.parameter.Parameter;
import com.epam.vs.fp.utility.parameter.WordNumberParameter;

public class WordNumberParameterConverter extends AbstractParameterConverter{
    public WordNumberParameterConverter() {
        super("word-number");
    }
    @Override
    protected Parameter internalConvert(String request) throws ParameterConversionException {
        if (request == null || request.isBlank()) {throw new ParameterConversionException(this.parameterName()+" cannot be nothing");}
        long words;
        try {
            words = Long.parseLong(request);
        } catch (NumberFormatException e) {
            throw new ParameterConversionException(parameterName + " must be a whole number");
        }
        if (words < 1) {throw new ParameterConversionException("word-number must be non-negative");}
        return new WordNumberParameter(words);
    }
}
