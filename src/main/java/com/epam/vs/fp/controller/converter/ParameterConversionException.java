package com.epam.vs.fp.controller.converter;

public class ParameterConversionException extends IllegalArgumentException{
    ParameterConversionException(String errorMessage) {
        super(errorMessage);
    }
    @SuppressWarnings("unused")
    ParameterConversionException(Exception errorMessage) {
        super(errorMessage.getMessage());
    }
}
