package com.epam.vs.fp.controller;

import com.epam.vs.fp.beans.PrintedProduct;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Optional;
import java.util.Queue;

interface SmallController {
    Optional<List<? extends PrintedProduct>> execute(Queue<String> commandQueue) throws InputMismatchException;
}
