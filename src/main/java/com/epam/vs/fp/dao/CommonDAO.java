package com.epam.vs.fp.dao;

import com.epam.vs.fp.beans.PrintedProduct;
import java.util.Collection;
import java.util.LinkedList;

class CommonDAO implements PrintedProductDAO<PrintedProduct>{
    private static final CommonDAO instance = new CommonDAO();
    public static PrintedProductDAO<PrintedProduct> getInstance() {
        return instance;
    }
    private CommonDAO(){}

    @Override
    public Collection<PrintedProduct> load() {
        Collection<PrintedProduct> entries = new LinkedList<>();
        for (DAOFactory factory: DAOFactory.values()) {
            if (factory.equals(DAOFactory.COMMON)){continue;}
            entries.addAll(factory.getDao().load());
        }
        return entries;
    }
}
