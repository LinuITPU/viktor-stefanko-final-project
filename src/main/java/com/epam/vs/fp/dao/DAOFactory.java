package com.epam.vs.fp.dao;

import com.epam.vs.fp.beans.PrintedProduct;

public enum DAOFactory {
    BOOK(BookDAO.getInstance()),
    DOCUMENT(DocumentDAO.getInstance()),
    PICTURE(PictureDAO.getInstance()),
    COMMON(CommonDAO.getInstance());
    private final PrintedProductDAO<? extends PrintedProduct> dao;

    DAOFactory(PrintedProductDAO<? extends PrintedProduct> dao) {
        this.dao = dao;
    }

    public PrintedProductDAO<? extends PrintedProduct> getDao() {
        return dao;
    }
}
