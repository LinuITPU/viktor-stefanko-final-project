package com.epam.vs.fp.dao;

import com.epam.vs.fp.beans.Document;

class DocumentDAO extends AbstractDAO<Document>{
    private static final DocumentDAO instance = new DocumentDAO();
    public static PrintedProductDAO<Document> getInstance() {
        return instance;
    }
    private DocumentDAO(){
        super("src/main/resources/documents.csv");
    }
    @Override
    public Document convertStrArrToEntity(String[] strEntry) {
        if (strEntry.length < 4) {
            throw new IndexOutOfBoundsException("Array strEntry must have at least 4 elements for type Document");
        }
        for (String testStr: strEntry) {
            if (testStr == null || testStr.isBlank()){throw new NullPointerException("fields cannot be blank");}
        }

        return new Document.Builder()
                .id(Integer.parseInt(strEntry[0]))
                .name(strEntry[1])
                .price(Double.parseDouble(strEntry[2]))
                .wordNumber(Long.parseLong(strEntry[3]))
                .build();
    }
}
