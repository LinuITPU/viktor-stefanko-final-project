package com.epam.vs.fp.dao;

import com.epam.vs.fp.beans.Book;


class BookDAO extends AbstractDAO<Book>{

    private static final BookDAO instance = new BookDAO();
    public static PrintedProductDAO<Book> getInstance() {
        return instance;
    }
    private BookDAO() {
        super("src/main/resources/books.csv");
    }

    @Override
    public Book convertStrArrToEntity(String[] strEntry) throws IndexOutOfBoundsException, NumberFormatException {
        if (strEntry.length < 6) {
            throw new IndexOutOfBoundsException("Array strEntry must have at least 6 elements for type Book");
        }
        for (String testStr: strEntry) {
            if (testStr == null || testStr.isBlank()){throw new NullPointerException("fields cannot be blank");}
        }

        return new Book.Builder()
                .id(Integer.parseInt(strEntry[0]))
                .name(strEntry[1])
                .price(Double.parseDouble(strEntry[2]))
                .wordNumber(Long.parseLong(strEntry[3]))
                .author(strEntry[4])
                .genre(strEntry[5])
                .build();
    }
}
