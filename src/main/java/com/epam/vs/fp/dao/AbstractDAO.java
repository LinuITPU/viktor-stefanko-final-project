package com.epam.vs.fp.dao;
import com.epam.vs.fp.beans.PrintedProduct;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

abstract class AbstractDAO <T extends PrintedProduct> implements PrintedProductDAO<T> {
    private final String FILEPATH;
    protected AbstractDAO(String filepath) {
        if (filepath == null || filepath.isBlank()) {throw new RuntimeException("Unable to find product database file as provided filepath is empty");}
        this.FILEPATH = filepath;
    }
    @Override
    public LinkedList<T> load(){
        LinkedList<T> entries = new LinkedList<>();
        FileReader fileReader;
        try {
            fileReader = new FileReader(FILEPATH);
        } catch (FileNotFoundException e) {
            return new LinkedList<>(); // if the file is not found, its content won't be included to the result of the method
        }
        CSVReader csvReader = new CSVReaderBuilder(fileReader).build();

        List<String[]> entriesStrings;
        try {
            entriesStrings = csvReader.readAll();
        } catch (IOException | CsvException e) {
            return entries; // return an empty list as it is not possible to load it
        }

        for(String[] stringEntry: entriesStrings.subList(1, entriesStrings.size())){
            try {
                entries.add(convertStrArrToEntity(stringEntry));
            } catch (IndexOutOfBoundsException | NullPointerException | IllegalArgumentException e) {
                continue; // do not add the entry if it is impossible to read it (it is written in an unacceptable format in te CSV file)
            }
        }
        return entries;
    }

    abstract T convertStrArrToEntity(String[] strEntry);

}
