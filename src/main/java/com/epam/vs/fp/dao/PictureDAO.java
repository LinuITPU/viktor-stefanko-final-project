package com.epam.vs.fp.dao;

import com.epam.vs.fp.beans.Colorful;
import com.epam.vs.fp.beans.Picture;

class PictureDAO extends AbstractDAO<Picture>{

    private static final PictureDAO instance = new PictureDAO();
    public static PrintedProductDAO<Picture> getInstance() {
        return instance;
    }
    private PictureDAO(){
        super("src/main/resources/pictures.csv");
    }

    @Override
    public Picture convertStrArrToEntity(String[] strEntry) {
        if (strEntry.length < 5) {
            throw new IndexOutOfBoundsException("Array strEntry must have at least 5 elements for type Picture");
        }
        for (String testStr: strEntry) {
            if (testStr == null || testStr.isBlank()){throw new NullPointerException("fields cannot be blank");}
        }
        return new Picture.Builder()
                .id(Integer.parseInt(strEntry[0]))
                .name(strEntry[1])
                .price(Double.parseDouble(strEntry[2]))
                .author(strEntry[3])
                .colorState(
                        switch (strEntry[4].toLowerCase()) {
                            case "colorful", "colourful" -> Colorful.Color.COLORFUL;
                            case "bnw" -> Colorful.Color.BNW;
                            default -> throw new IllegalArgumentException();
                        }
                )
                .genre(strEntry[5]).build();
    }
}
