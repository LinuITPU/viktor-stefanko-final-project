package com.epam.vs.fp.dao;
import com.epam.vs.fp.beans.PrintedProduct;
import java.util.Collection;

public interface PrintedProductDAO<T extends PrintedProduct>{
    Collection<T> load();
}
