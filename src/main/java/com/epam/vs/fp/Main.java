package com.epam.vs.fp;

import com.epam.vs.fp.beans.PrintedProduct;
import com.epam.vs.fp.controller.FrontController;
import com.epam.vs.fp.controller.FrontControllerImpl;
import com.epam.vs.fp.view.ViewPresenter;
import com.epam.vs.fp.view.WarehousePresenter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        FrontController activeController = FrontControllerImpl.getINSTANCE();
        ViewPresenter<PrintedProduct> presenter = WarehousePresenter.getINSTANCE();
        presenter.printIntro();
        Scanner sc = new Scanner(System.in);
        while (true) {
            String command = sc.nextLine();
            if (!activeController.listen(command)) {
                break;
            }
        }
    }
}
