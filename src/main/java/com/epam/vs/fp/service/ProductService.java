package com.epam.vs.fp.service;
import com.epam.vs.fp.beans.PrintedProduct;
import com.epam.vs.fp.utility.criteria.SearchCriteria;
import com.epam.vs.fp.utility.comparator.ComparatorFactory;

import java.util.Collection;
import java.util.List;

@SuppressWarnings("rawtypes")
public interface ProductService {
    <T extends PrintedProduct> Collection<T> filter(List<T> list, SearchCriteria criteria);
    <T extends PrintedProduct> List<T> sort(List<T> products, ComparatorFactory factory);
    List<PrintedProduct> getDB();
    <A>List<A> reverse(List<A> list);
}
