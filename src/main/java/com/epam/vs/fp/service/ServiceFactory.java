package com.epam.vs.fp.service;

import com.epam.vs.fp.beans.PrintedProduct;
import com.epam.vs.fp.dao.DAOFactory;
import com.epam.vs.fp.utility.criteria.SearchCriteria;
import com.epam.vs.fp.utility.comparator.ProductComparator;
import com.epam.vs.fp.utility.comparator.ComparatorFactory;
import java.util.*;

public enum ServiceFactory {
    INSTANCE;

    @SuppressWarnings({"unchecked", "rawtypes"})
    public ProductService getProductService() {
        return new ProductService() {

            @Override
            public List<PrintedProduct> getDB(){
                return (LinkedList<PrintedProduct>) DAOFactory.COMMON.getDao().load();
            }

            @Override
            public <A> List<A> reverse(List<A> list) {
                List reverseList = new LinkedList(list);
                Collections.reverse(reverseList);
                return reverseList;
            }

            @Override
            public <T extends PrintedProduct> List<T> sort(List<T> products_, ComparatorFactory comFactory) {
                List<T> products = new LinkedList<>(products_);
                List<T> notSuitableObjects = new LinkedList<>();
                Iterator<T> iterator = products.iterator();
                ProductComparator com = comFactory.getComparator();
                while (iterator.hasNext()) {
                    T current = iterator.next();
                    if (!com.isSuitable(current)) {
                        notSuitableObjects.add(current);
                        iterator.remove();
                    }
                }
                products.sort((Comparator<? super T>) comFactory.getComparator());
                products.addAll(notSuitableObjects);
                return products;
            }

            @Override
            public <T extends PrintedProduct>List filter(List<T> list, SearchCriteria criteria) {
                List<PrintedProduct> filteredEntries = new LinkedList<>();

                for (T product: list) {
                    if (criteria.test(product)) {
                        filteredEntries.add(product);
                    }
                }
                return filteredEntries;
            }
        };
    }
}
