package com.epam.vs.fp.beans;

import java.util.Objects;

public class Document extends PrintedProduct implements hasWords {
    private final long wordNumber;

    public Document(Builder b) {
        super(b);
        if (b.wordNumber < 0) {throw new IllegalArgumentException("wordNumber cannot be negative");}
        this.wordNumber = b.wordNumber;
    }


    public static class Builder extends PrintedProduct.Builder<Builder, Document> {
        private long wordNumber;

        public Builder wordNumber(long wordNumber) {
            if (wordNumber < 0) {throw new IllegalArgumentException("wordNumber must be non-negative");}
            this.wordNumber = wordNumber;
            return this;
        }

        @Override
        public Document build() {
            return new Document(this);
        }
    }

    @Override
    public long getWordNumber() {
        return wordNumber;
    }


    @Override
    public String toString() {
        return "Document{" +
                "id=" + getId() +
                ", name='" + getName() + '\'' +
                ", price=" + getPrice() +
                ", wordNumber=" + wordNumber +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Document document = (Document) o;
        return wordNumber == document.wordNumber;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), wordNumber);
    }
}
