package com.epam.vs.fp.beans;

public interface hasAuthor {
    String getAuthor();
}
