package com.epam.vs.fp.beans;

import java.util.Objects;

public class Book extends PrintedProduct implements hasWords, hasAuthor, hasGenre{
    private final long wordNumber;
    private final String author;
    private final String genre;

    public Book(Builder b) {
        super(b);
        if (b.genre == null || b.genre.isBlank()) {throw new NullPointerException("genre cannot be nothing");}
        if (b.author == null || b.author.isBlank()) {throw new NullPointerException("author cannot be nothing");}
        if (b.wordNumber < 0) {throw new IllegalArgumentException("wordNumber cannot be negative");}
        this.genre = b.genre;
        this.author = b.author;
        this.wordNumber = b.wordNumber;
    }

    public static class Builder extends PrintedProduct.Builder<Builder, Book> {
        private long wordNumber;
        private String genre;
        private String author;

        public Builder wordNumber(long wordNumber) {
            if (wordNumber < 0) {throw new IllegalArgumentException("wordNumber must be non-negative");}
            this.wordNumber = wordNumber;
            return this;
        }

        public Builder genre(String genre) {
            if (genre == null || genre.isBlank()) {throw new NullPointerException("genre must have a value");}
            this.genre = genre;
            return this;
        }

        public Builder author(String author) {
            if (author == null || author.isBlank()) {throw new NullPointerException("author must have a value");}
            this.author = author;
            return this;
        }

        @Override
        public Book build() {
            return new Book(this);
        }
    }

    @Override
    public long getWordNumber() {
        return wordNumber;
    }


    @Override
    public String getAuthor() {
        return author;
    }


    @Override
    public String getGenre() {
        return genre;
    }


    @Override
    public String toString() {
        return "Book{" +
                "id=" + getId() +
                ", name='" + getName() + '\'' +
                ", price=" + getPrice() +
                ", wordNumber=" + wordNumber +
                ", author='" + author + '\'' +
                ", genre='" + genre + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Book book = (Book) o;
        return wordNumber == book.wordNumber && Objects.equals(author, book.author) && Objects.equals(genre, book.genre);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), wordNumber, author, genre);
    }
}
