package com.epam.vs.fp.beans;

import java.util.Objects;

public class Picture extends PrintedProduct implements hasAuthor, hasGenre, Colorful{
    private final String author;
    private final Color colorState;
    private final String genre;

    public Picture(Builder b) {
        super(b);
        if (b.colorState == null) {throw new NullPointerException("colorState must be specified");}
        if (b.genre == null || b.genre.isBlank()) {throw new NullPointerException("genre cannot be nothing");}
        if (b.author == null || b.author.isBlank()) {throw new NullPointerException("author cannot be nothing");}
        this.colorState = b.colorState;
        this.genre = b.genre;
        this.author = b.author;
    }

    public static class Builder extends PrintedProduct.Builder<Builder, Picture> {
        private Color colorState;
        private String genre;
        private String author;

        public Builder colorState(Color colorState) {
            if (colorState == null) {throw new NullPointerException("colorState must not be null");}
            this.colorState = colorState;
            return this;
        }

        public Builder genre(String genre) {
            if (genre == null || genre.isBlank()) {throw new NullPointerException("genre must have a value");}
            this.genre = genre;
            return this;
        }

        public Builder author(String author) {
            if (author == null || author.isBlank()) {throw new NullPointerException("author must have a value");}
            this.author = author;
            return this;
        }

        @Override
        public Picture build() {
            return new Picture(this);
        }
    }

    @Override
    public String getAuthor() {
        return author;
    }


    @Override
    public String getGenre() {
        return genre;
    }

    @Override
    public Color getColorState() {
        return colorState;
    }

    @Override
    public String toString() {
        return "Picture{" +
                "id=" + getId() +
                ", name='" + getName() + '\'' +
                ", price=" + getPrice() +
                ", author='" + author + '\'' +
                ", colorState=" + colorState +
                ", genre='" + genre + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Picture picture = (Picture) o;
        return Objects.equals(author, picture.author) && colorState == picture.colorState && Objects.equals(genre, picture.genre);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), author, colorState, genre);
    }
}
