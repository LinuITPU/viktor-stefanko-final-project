package com.epam.vs.fp.beans;

public interface Colorful {
    enum Color {
        BNW(2),
        COLORFUL(1);

        private final int value;
        Color(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
    Color getColorState();


}
