package com.epam.vs.fp.beans;


import java.util.Objects;

public abstract class PrintedProduct {
    private final int id;
    private final String name;
    private final double price;

    public PrintedProduct(Builder<? extends ObjBuilder<?>, ? extends PrintedProduct> b) {
        super();
        if (b.name == null || b.name.isBlank()) {throw new NullPointerException("name cannot be nothing");}
        if (b.id < 0) {throw new NullPointerException("id cannot be less than 0");}
        if (b.price < 0) {throw new NullPointerException("price cannot be less than 0");}
        this.id = b.id;
        this.name = b.name;
        this.price = b.price;

    }

    public static abstract class Builder<SELF extends Builder<SELF, T>, T> implements ObjBuilder<T> {
        private int id;
        private String name;
        private double price;

        @SuppressWarnings("unchecked")
        public SELF id(int id) {
            if (id < 0) {throw new IllegalArgumentException("id cannot be less than 0");}
            this.id = id;
            return (SELF) this;
        }
        @SuppressWarnings("unchecked")
        public SELF name(String name) {
            if (name == null || name.isBlank()) {throw new NullPointerException("name must have a value");}
            this.name = name;
            return (SELF) this;
        }
        @SuppressWarnings("unchecked")
        public SELF price(double price) {
            if (price < 0) {throw new IllegalArgumentException("price cannot be less than 0");}
            this.price = price;
            return (SELF) this;
        }
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "PrintedProduct{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PrintedProduct product = (PrintedProduct) o;
        return id == product.id && Double.compare(price, product.price) == 0 && Objects.equals(name, product.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, price);
    }
}
