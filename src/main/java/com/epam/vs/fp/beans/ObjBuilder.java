package com.epam.vs.fp.beans;

public interface ObjBuilder<T> {
    T build();
}
