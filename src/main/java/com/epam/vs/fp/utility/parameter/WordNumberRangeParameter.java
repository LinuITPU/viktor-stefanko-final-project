package com.epam.vs.fp.utility.parameter;

import com.epam.vs.fp.beans.PrintedProduct;
import com.epam.vs.fp.beans.hasWords;

public record WordNumberRangeParameter(Range<Long> range) implements Parameter {
    public WordNumberRangeParameter {
        if (range.getLow() <= 0){throw new IllegalArgumentException("word-number must be positive");}
    }

    @Override
    public boolean test(PrintedProduct product) {
        if (product instanceof hasWords) {
            return range.isIn(((hasWords) product).getWordNumber());
        }  else {
            throw new ClassCastException("product does not have word-number property");
        }
    }
}
