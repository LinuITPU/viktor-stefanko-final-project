package com.epam.vs.fp.utility.criteria;

import com.epam.vs.fp.beans.PrintedProduct;
import com.epam.vs.fp.utility.parameter.Parameter;

import java.util.HashMap;
import java.util.Map;

public class CommonSearchCriteria implements SearchCriteria<PrintedProduct>{

    final Map<Class<?>, Parameter> parameters = new HashMap<>();

    @Override
    public <P extends Parameter> void add(P param) {
        parameters.put(param.getClass(), param);
    }

    @Override
    public boolean test(PrintedProduct product) {
        for (Parameter parameter: parameters.values()) {
            try {
                if (! parameter.test(product)) {
                    return false;
                }
            } catch (ClassCastException e) { // the product fails the test as it is of a different type
                return false;
            }

        }
        return true;
    }
}
