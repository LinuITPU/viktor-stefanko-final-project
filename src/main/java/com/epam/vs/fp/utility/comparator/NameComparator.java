package com.epam.vs.fp.utility.comparator;

import com.epam.vs.fp.beans.PrintedProduct;

public record NameComparator() implements ProductComparator<PrintedProduct> {
    @Override
    public int compare(PrintedProduct p1, PrintedProduct p2) {
        return CharSequence.compare(p1.getName(), p2.getName());
    }

    @Override
    public boolean isSuitable(Object o) {
        return o instanceof PrintedProduct;
    }
}
