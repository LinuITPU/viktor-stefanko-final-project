package com.epam.vs.fp.utility.parameter;
import com.epam.vs.fp.beans.hasAuthor;
import com.epam.vs.fp.beans.PrintedProduct;

public record AuthorParameter(String author) implements Parameter {
    public AuthorParameter {
        if (author == null || author.isBlank()){throw new NullPointerException();}
    }
    @Override
    public boolean test(PrintedProduct product) throws ClassCastException {
        if (product instanceof hasAuthor) {
            return ((hasAuthor) product).getAuthor().equals(author);
        } else {
            throw new ClassCastException("product does not have author property");
        }
    }
}
