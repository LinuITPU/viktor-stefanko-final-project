package com.epam.vs.fp.utility.comparator;
import com.epam.vs.fp.beans.hasAuthor;

public record AuthorComparator() implements ProductComparator<hasAuthor> {
    @Override
    public int compare(hasAuthor p1, hasAuthor p2) {
        return CharSequence.compare(p1.getAuthor(), p2.getAuthor());
    }

    @Override
    public boolean isSuitable(Object o) {
        return o instanceof hasAuthor;
    }
}
