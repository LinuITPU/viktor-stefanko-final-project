package com.epam.vs.fp.utility.parameter;

import com.epam.vs.fp.beans.PrintedProduct;

public record TypeParameter(String type) implements Parameter {
    public TypeParameter {
        if (type == null || type.isBlank()){throw new NullPointerException();}
    }
    @Override
    public boolean test(PrintedProduct product) {
        return type.equals(product.getClass().getSimpleName());
    }
}
