package com.epam.vs.fp.utility.comparator;

import com.epam.vs.fp.beans.PrintedProduct;

public record IdComparator() implements ProductComparator<PrintedProduct> {
    @Override
    public int compare(PrintedProduct p1, PrintedProduct p2) {
        return Integer.compare(p1.getId(), p2.getId());
    }


    @Override
    public boolean isSuitable(Object o) {
        return o instanceof PrintedProduct;
    }
}
