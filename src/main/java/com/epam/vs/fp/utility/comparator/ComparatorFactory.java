package com.epam.vs.fp.utility.comparator;

public enum ComparatorFactory {
    AUTHOR(new AuthorComparator()),
    COLOR(new ColorComparator()),
    ID(new IdComparator()),
    NAME(new NameComparator()),
    PRICE(new PriceComparator()),
    WORD_NUMBER(new WordNumberComparator()),
    GENRE(new GenreComparator()),
    TYPE(new TypeComparator());

    private final ProductComparator<?> comparator;
    ComparatorFactory(ProductComparator<?> com) {
        this.comparator = com;
    }

    public ProductComparator<?> getComparator() {
        return comparator;
    }
}
