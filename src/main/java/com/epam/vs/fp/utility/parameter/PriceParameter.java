package com.epam.vs.fp.utility.parameter;

import com.epam.vs.fp.beans.PrintedProduct;

public record PriceParameter(double price) implements Parameter {
    public PriceParameter {
        if (price < 0){throw new IllegalArgumentException("price cannot be negative");}
    }
    @Override
    public boolean test(PrintedProduct product) {
        return price == product.getPrice();
    }
}
