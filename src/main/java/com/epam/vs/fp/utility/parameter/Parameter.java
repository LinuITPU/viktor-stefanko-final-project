package com.epam.vs.fp.utility.parameter;

import com.epam.vs.fp.beans.PrintedProduct;

public interface Parameter{
    boolean test(PrintedProduct product) throws ClassCastException;
}
