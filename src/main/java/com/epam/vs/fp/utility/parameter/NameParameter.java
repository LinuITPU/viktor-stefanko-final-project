package com.epam.vs.fp.utility.parameter;

import com.epam.vs.fp.beans.PrintedProduct;

public record NameParameter(String name) implements Parameter {
    public NameParameter {
        if (name == null || name.isBlank()){throw new NullPointerException();}
    }
    @Override
    public boolean test(PrintedProduct product) {
        return name.equals(product.getName());
    }
}
