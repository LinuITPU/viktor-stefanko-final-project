package com.epam.vs.fp.utility.parameter;
import com.epam.vs.fp.beans.Colorful;
import com.epam.vs.fp.beans.PrintedProduct;

public record ColorParameter(Colorful.Color color) implements Parameter {
    public ColorParameter {
        if (color == null){throw new NullPointerException();}
    }
    @Override
    public boolean test(PrintedProduct product) throws ClassCastException {
        if (product instanceof Colorful) {
            return ((Colorful) product).getColorState().equals(color);
        }  else {
            throw new ClassCastException("product does not have color property");
        }
    }
}
