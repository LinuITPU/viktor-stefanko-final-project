package com.epam.vs.fp.utility.comparator;

import com.epam.vs.fp.beans.Colorful;

public record ColorComparator() implements ProductComparator<Colorful> {
    @Override
    public int compare(Colorful p1, Colorful p2) {
        return Integer.compare(p1.getColorState().getValue(), p2.getColorState().getValue());
    }


    @Override
    public boolean isSuitable(Object o) {
        return o instanceof Colorful;
    }
}
