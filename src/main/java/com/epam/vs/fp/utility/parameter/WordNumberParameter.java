package com.epam.vs.fp.utility.parameter;

import com.epam.vs.fp.beans.PrintedProduct;
import com.epam.vs.fp.beans.hasWords;

public record WordNumberParameter(long wordNumber) implements Parameter {
    public WordNumberParameter {
        if (wordNumber < 0){throw new IllegalArgumentException("word-number cannot be negative");}
    }
    @Override
    public boolean test(PrintedProduct product) throws ClassCastException {
        if (product instanceof hasWords) {
            return ((hasWords) product).getWordNumber() == wordNumber;
        }  else {
            throw new ClassCastException("product does not have word-number property");
        }
    }
}
