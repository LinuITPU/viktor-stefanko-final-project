package com.epam.vs.fp.utility.parameter;

import java.util.Objects;

public record Range<T extends Comparable<T>>(T low, T high) {
    public Range {
        if (low == null || high == null) {
            throw new NullPointerException("low and high cannot be nothing");
        }
        if (low.compareTo(high) > 0) {
            throw new IllegalArgumentException("high cannot be smaller than low");
        }
    }

    public T getLow() {
        return low;
    }
    @SuppressWarnings("unused")
    public T getHigh() {
        return high;
    }

    public boolean isIn(T value) {
        boolean within = true;
        Objects.requireNonNull(value);
        if (low != null)
            within = low.compareTo(value) <= 0;
        if (high != null)
            within &= value.compareTo(high) <= 0;
        return within;
    }
}
