package com.epam.vs.fp.utility.parameter;

import com.epam.vs.fp.beans.PrintedProduct;

public record PriceRangeParameter(Range<Double> range) implements Parameter {

    public PriceRangeParameter {
        if (range.getLow() < 0){throw new IllegalArgumentException("price cannot be negative");}
    }

    @Override
    public boolean test(PrintedProduct product) {
        return range.isIn(product.getPrice());
    }
}
