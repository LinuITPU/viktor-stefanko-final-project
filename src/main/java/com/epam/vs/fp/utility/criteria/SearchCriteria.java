package com.epam.vs.fp.utility.criteria;

import com.epam.vs.fp.utility.parameter.Parameter;
import com.epam.vs.fp.beans.PrintedProduct;

public interface SearchCriteria<T extends PrintedProduct> {
    <P extends Parameter> void add(P parameter);
    boolean test(T testedProduct);
}
