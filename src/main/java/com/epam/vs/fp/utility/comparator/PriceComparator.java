package com.epam.vs.fp.utility.comparator;

import com.epam.vs.fp.beans.PrintedProduct;

public record PriceComparator() implements ProductComparator<PrintedProduct> {
    @Override
    public int compare(PrintedProduct p1, PrintedProduct p2) {
        return Double.compare(p1.getPrice(), p2.getPrice());
    }

    @Override
    public boolean isSuitable(Object o) {
        return o instanceof PrintedProduct;
    }
}
