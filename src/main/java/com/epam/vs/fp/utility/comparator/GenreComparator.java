package com.epam.vs.fp.utility.comparator;
import com.epam.vs.fp.beans.hasGenre;

public record GenreComparator() implements ProductComparator<hasGenre> {
    @Override
    public int compare(hasGenre p1, hasGenre p2) {
        return CharSequence.compare(p1.getGenre(), p2.getGenre());
    }

    @Override
    public boolean isSuitable(Object o) {
        return o instanceof hasGenre;
    }
}
