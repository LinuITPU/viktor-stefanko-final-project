package com.epam.vs.fp.utility.comparator;

import com.epam.vs.fp.beans.hasWords;

public record WordNumberComparator() implements ProductComparator<hasWords> {
    @Override
    public int compare(hasWords p1, hasWords p2) {
        return Long.compare(p1.getWordNumber(), p2.getWordNumber());
    }

    @Override
    public boolean isSuitable(Object o) {
        return o instanceof hasWords;
    }
}
