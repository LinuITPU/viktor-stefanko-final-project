package com.epam.vs.fp.utility.parameter;

import com.epam.vs.fp.beans.PrintedProduct;
import com.epam.vs.fp.beans.hasGenre;

public record GenreParameter(String genre) implements Parameter {
    public GenreParameter {
        if (genre == null || genre.isBlank()){throw new NullPointerException();}
    }
    @Override
    public boolean test(PrintedProduct product) throws ClassCastException {
        if (product instanceof hasGenre) {
            return ((hasGenre) product).getGenre().equals(genre);
        }  else {
            throw new ClassCastException("product does not have genre property");
        }
    }
}
