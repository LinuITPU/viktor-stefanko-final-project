package com.epam.vs.fp.utility.comparator;

import com.epam.vs.fp.beans.PrintedProduct;

public record TypeComparator() implements ProductComparator<PrintedProduct> {
    @Override
    public int compare(PrintedProduct p1, PrintedProduct p2) {
        return CharSequence.compare(p1.getClass().getSimpleName(), p2.getClass().getSimpleName());
    }

    @Override
    public boolean isSuitable(Object o) {
        return o instanceof PrintedProduct;
    }
}
