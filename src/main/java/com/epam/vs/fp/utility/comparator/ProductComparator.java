package com.epam.vs.fp.utility.comparator;

import java.util.Comparator;

public interface ProductComparator<T> extends Comparator<T> {
    boolean isSuitable(Object o); // is the object of correct type and has the property to be compared
}
