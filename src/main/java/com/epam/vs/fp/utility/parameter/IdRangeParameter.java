package com.epam.vs.fp.utility.parameter;

import com.epam.vs.fp.beans.PrintedProduct;

public record IdRangeParameter(Range<Integer> range) implements Parameter {

    public IdRangeParameter {
        if (range.getLow() < 0){throw new IllegalArgumentException("id cannot be negative");}
    }

    @Override
    public boolean test(PrintedProduct product) {
        return range.isIn(product.getId());
    }
}