package com.epam.vs.fp.utility.parameter;

import com.epam.vs.fp.beans.PrintedProduct;

public record IdParameter(int id) implements Parameter {
    public IdParameter {
        if (id < 0){throw new IllegalArgumentException("id cannot be negative");}
    }
    @Override
    public boolean test(PrintedProduct product) {
        return id == product.getId();
    }
}
