package com.epam.vs.fp.view.column;

import com.epam.vs.fp.beans.PrintedProduct;
import com.epam.vs.fp.beans.hasGenre;
import com.epam.vs.fp.utility.comparator.ComparatorFactory;

public class GenreColumn implements Column {
    @Override
    public int getColumnLength() {
        return 15;
    }

    @Override
    public String getColumnName() {
        return "GENRE";
    }

    @Override
    public ComparatorFactory getColumnComparatorFactory() {
        return ComparatorFactory.GENRE;
    }

    @Override
    public <T extends PrintedProduct> String getStringValue(T product) {
        return product instanceof hasGenre ? ((hasGenre) product).getGenre() : "-";
    }
}
