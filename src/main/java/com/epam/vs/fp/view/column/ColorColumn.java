package com.epam.vs.fp.view.column;

import com.epam.vs.fp.beans.Colorful;
import com.epam.vs.fp.beans.PrintedProduct;
import com.epam.vs.fp.utility.comparator.ComparatorFactory;

public class ColorColumn implements Column {
    @Override
    public int getColumnLength() {
        return 11;
    }

    @Override
    public String getColumnName() {
        return "COLOR";
    }

    @Override
    public ComparatorFactory getColumnComparatorFactory() {
        return ComparatorFactory.COLOR;
    }

    @Override
    public <T extends PrintedProduct> String getStringValue(T product) {
        return product instanceof Colorful ? ((Colorful) product).getColorState().toString() : "-";
    }
}
