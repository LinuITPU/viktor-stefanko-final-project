package com.epam.vs.fp.view;

import com.epam.vs.fp.beans.*;
import com.epam.vs.fp.view.column.ColumnFactory;
import com.epam.vs.fp.view.column.Column;

import java.util.Collection;
import java.util.InputMismatchException;
import java.util.Iterator;

public class WarehousePresenter implements ViewPresenter<PrintedProduct> {
    private static final WarehousePresenter INSTANCE = new WarehousePresenter();

    public static WarehousePresenter getINSTANCE() {
        return INSTANCE;
    }
    private WarehousePresenter(){}

    private String prepareCell(String value, int length) {
        if (length < 3) {throw new IndexOutOfBoundsException("a cell cannot be shorter then 3 units");}
        int availableSpace = length - 3;
        int whitespaceLength = availableSpace - value.length();
        if (whitespaceLength < 0) {
            value = value.substring(0, availableSpace - 3) + "...";
            whitespaceLength = 0;
        }
        return "| " +
                value +
                new String(new char[whitespaceLength]).replace("\0", " ") +
                " ";
    }

    private String prepareCell(String value, int length, boolean isAscending) {
        value = value + " " + (isAscending ? "↓" : "↑");
        return prepareCell(value, length);
    }

    private int calcTableWidth() {
        int totalLength = 1;
        for (ColumnFactory col: ColumnFactory.values()) {
            totalLength += col.getColumn().getColumnLength();
        }
        return totalLength;
    }

    public void outputEmpty() {
        int totalLength = calcTableWidth();
        System.out.println(new String(new char[totalLength]).replace("\0", "="));
        System.out.println(prepareCell("Nothing was found", totalLength - 1) + "|");
        System.out.println(new String(new char[totalLength]).replace("\0", "="));

    }

    @Override
    public void outputEntries(Collection<? extends PrintedProduct> entries, Column sortColumn, boolean isAscending) {
        if (entries == null) {throw new NullPointerException("entries cannot be null");}
        if (sortColumn == null) {throw new NullPointerException("sortColumn must be specified and cannot be null");}

        if (entries.isEmpty()) {
            outputEmpty();
            return;
        }

        int totalLength = calcTableWidth();
        System.out.println(new String(new char[totalLength]).replace("\0", "="));
        for (ColumnFactory col: ColumnFactory.values()) {
            Column colType = col.getColumn();
            if (colType.equals(sortColumn)) {
                System.out.print(prepareCell(colType.getColumnName(), colType.getColumnLength(), isAscending));
            } else {
                System.out.print(prepareCell(colType.getColumnName(), colType.getColumnLength()));
            }
        }
        System.out.print("|\n");
        System.out.println(new String(new char[totalLength]).replace("\0", "-"));

        Iterator<? extends PrintedProduct> iterator = entries.iterator();
        int number = 0;
        while (iterator.hasNext()) {
            PrintedProduct product = iterator.next();
            number++;
            System.out.print(
                    prepareCell(String.valueOf(number), ColumnFactory.NUMBER.getColumn().getColumnLength())
            );
            for (ColumnFactory propertyColumn: ColumnFactory.values()) {
                if(propertyColumn.equals(ColumnFactory.NUMBER)) {continue;}
                System.out.print(prepareCell(
                        propertyColumn.getColumn().getStringValue(product),
                        propertyColumn.getColumn().getColumnLength())
                );
            }
            System.out.print("|\n");
        }
        System.out.println(new String(new char[totalLength]).replace("\0", "="));

    }

    @Override
    public void presentException(Exception e) {
        if (e instanceof InputMismatchException) {
            System.out.println("Invalid input: "+e.getMessage());
        } else {
            System.out.println("Oops. We've run into an issue:\n"+e.getMessage());
        }
    }

    @Override
    public void printHelp() {
        System.out.println("""
                List of available commands:
                -------------
                browse - displays a table of all the products in the warehouse
                exit - exits the application
                sort <property> <order> - sets sorting order
                search <parameter> <value> ... - filters products based on their properties
                search <parameter> range <number> <value> ... - filters products depending wether their property is in some number range
                help - outputs a list of all available commands
                help <command> - outputs an instruction for using a specific command
                -------------
                Some of the arguments may be case sensitive.""");
    }

    @Override
    public void printHelp(String command) {
        if (command == null || command.isBlank()) {throw new NullPointerException("command not entered");}
        command = command.toLowerCase();
        String response = switch (command) {
            case "help" -> "help - outputs a list of all available commands if used without additional parameters\n" +
                    "help <command> - outputs an instruction for using a specific command";
            case "browse" -> "browse - displays a table of all the products in the warehouse";
            case "sort" -> """
                    sort - sets sorting order for entries in the table of products.
                    Specifying a parameter (the column name to be sorted based on) is required.
                    The default sorting order in ascending. It can be changed to descending if 'descending' or 'desc' is written after the parameter name.
                    """;
            case "search" -> """
                    search - filters products based on their properties, which are specified as parameters
                    after the keyword 'search', a search parameter should be specified (the names of which correlate with the column names) and a value to it
                    A few search parameters can be combined, like: 'search price 100 word-number 345'
                    
                    search can also be used to filter products based on their numeric properties if they fall in some number range.
                    To do that, add 'range' after the NUMERIC property and specify exactly 2 numbers: lowest and highest allowed values.""";
            case "exit" -> "exit - exits the application.";
            default -> throw new InputMismatchException("'"+command+"'" + " command does not exist");
        };
        System.out.println(response);
    }
    @Override
    public void printIntro() {
        System.out.println("""
                PRINTED PRODUCTS WAREHOUSE SEARCH SYSTEM
                Beta 2 - created on May 09 - 2024
                - VIKTOR STEFANKO
                """);
        printHelp();
    }
}
