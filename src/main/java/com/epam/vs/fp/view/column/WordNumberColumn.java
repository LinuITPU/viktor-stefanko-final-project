package com.epam.vs.fp.view.column;

import com.epam.vs.fp.beans.PrintedProduct;
import com.epam.vs.fp.beans.hasWords;
import com.epam.vs.fp.utility.comparator.ComparatorFactory;

public class WordNumberColumn implements Column {
    @Override
    public int getColumnLength() {
        return 16;
    }

    @Override
    public String getColumnName() {
        return "WORD-NUMBER";
    }

    @Override
    public ComparatorFactory getColumnComparatorFactory() {
        return ComparatorFactory.WORD_NUMBER;
    }

    @Override
    public <T extends PrintedProduct> String getStringValue(T product) {
        return product instanceof hasWords ? String.valueOf(((hasWords) product).getWordNumber()) : "-";
    }
}
