package com.epam.vs.fp.view.column;

import com.epam.vs.fp.beans.PrintedProduct;
import com.epam.vs.fp.beans.hasAuthor;
import com.epam.vs.fp.utility.comparator.ComparatorFactory;

public class AuthorColumn implements Column {
    @Override
    public int getColumnLength() {
        return 11;
    }

    @Override
    public String getColumnName() {
        return "AUTHOR";
    }

    @Override
    public ComparatorFactory getColumnComparatorFactory() {
        return ComparatorFactory.AUTHOR;
    }

    @Override
    public <T extends PrintedProduct> String getStringValue(T product) {
        return product instanceof hasAuthor ? ((hasAuthor) product).getAuthor() : "-";
    }
}
