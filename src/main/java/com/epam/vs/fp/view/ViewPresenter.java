package com.epam.vs.fp.view;
import com.epam.vs.fp.view.column.Column;

import java.util.Collection;

public interface  ViewPresenter <T>{
    void outputEntries(Collection<? extends T> entries, Column sortColumn, boolean isAscending);
    void outputEmpty();

    void presentException(Exception e);
    void printHelp();
    void printHelp(String command);
    void printIntro();
}
