package com.epam.vs.fp.view.column;

import com.epam.vs.fp.beans.PrintedProduct;
import com.epam.vs.fp.utility.comparator.ComparatorFactory;

public class IdColumn implements Column {
    @Override
    public int getColumnLength() {
        return 9;
    }

    @Override
    public String getColumnName() {
        return "ID";
    }

    @Override
    public ComparatorFactory getColumnComparatorFactory() {
        return ComparatorFactory.ID;
    }

    @Override
    public <T extends PrintedProduct> String getStringValue(T product) {
        return String.valueOf(product.getId());
    }
}
