package com.epam.vs.fp.view.column;

import com.epam.vs.fp.beans.PrintedProduct;
import com.epam.vs.fp.utility.comparator.ComparatorFactory;

public class PriceColumn implements Column {
    private final String CURRENCY = "$";
    @Override
    public int getColumnLength() {
        return 15;
    }

    @Override
    public String getColumnName() {
        return "PRICE";
    }

    @Override
    public ComparatorFactory getColumnComparatorFactory() {
        return ComparatorFactory.PRICE;
    }

    @Override
    public <T extends PrintedProduct> String getStringValue(T product) {
        return CURRENCY + String.format("%.2f", product.getPrice());
    }
}
