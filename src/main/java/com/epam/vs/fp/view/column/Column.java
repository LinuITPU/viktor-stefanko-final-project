package com.epam.vs.fp.view.column;

import com.epam.vs.fp.beans.PrintedProduct;
import com.epam.vs.fp.utility.comparator.ComparatorFactory;

public interface Column {
    int getColumnLength();

    String getColumnName();

    ComparatorFactory getColumnComparatorFactory();
    <T extends PrintedProduct>String getStringValue(T product);
}
