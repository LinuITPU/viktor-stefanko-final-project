package com.epam.vs.fp.view.column;

public enum ColumnFactory {
    NUMBER(new EnrtyNumberColumn()),
    TYPE(new TypeColumn()),
    NAME(new NameColumn()),
    PRICE(new PriceColumn()),
    ID(new IdColumn()),
    AUTHOR(new AuthorColumn()),
    GENRE(new GenreColumn()),
    WORD_LENGTH(new WordNumberColumn()),
    COLOR(new ColorColumn());

    private final Column column;
    ColumnFactory(Column column) {
        this.column = column;

    }

    public Column getColumn() {
        return column;
    }
}
