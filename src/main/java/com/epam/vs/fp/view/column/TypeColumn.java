package com.epam.vs.fp.view.column;

import com.epam.vs.fp.beans.PrintedProduct;
import com.epam.vs.fp.utility.comparator.ComparatorFactory;

public class TypeColumn implements Column {
    @Override
    public int getColumnLength() {
        return 15;
    }

    @Override
    public String getColumnName() {
        return "TYPE";
    }

    @Override
    public ComparatorFactory getColumnComparatorFactory() {
        return ComparatorFactory.TYPE;
    }

    @Override
    public <T extends PrintedProduct> String getStringValue(T product) {
        return product.getClass().getSimpleName();
    }
}
