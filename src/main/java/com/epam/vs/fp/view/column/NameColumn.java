package com.epam.vs.fp.view.column;

import com.epam.vs.fp.beans.PrintedProduct;
import com.epam.vs.fp.utility.comparator.ComparatorFactory;

public class NameColumn implements Column {
    @Override
    public int getColumnLength() {
        return 29;
    }

    @Override
    public String getColumnName() {
        return "NAME";
    }

    @Override
    public ComparatorFactory getColumnComparatorFactory() {
        return ComparatorFactory.NAME;
    }

    @Override
    public <T extends PrintedProduct> String getStringValue(T product) {
        return product.getName();
    }
}
