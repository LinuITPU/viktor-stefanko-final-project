package com.epam.vs.fp.view.column;

import com.epam.vs.fp.beans.PrintedProduct;
import com.epam.vs.fp.utility.comparator.ComparatorFactory;

public class EnrtyNumberColumn implements Column {
    @Override
    public int getColumnLength() {
        return 7;
    }

    @Override
    public String getColumnName() {
        return "#";
    }

    @Override
    public ComparatorFactory getColumnComparatorFactory() {
        return null;
    }

    @Override
    public <T extends PrintedProduct> String getStringValue(T product) {
        return null;
    }
}
